#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"
#include <gmp.h>

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)

// x is the number to set len is the # of data words to be
// read from our buffer '-1' for kinda endianness. It
// determines whether or not the first word read is the most
// significant or least significant word.  '1' for words of
// size '1 byte' '0' to use the native endianness of the
// CPU. However, you can also use '1' and '-1' to 'force'
// the endianness of the bytes.  '0' to skip '0' bytes of
// each word. Why wouldn't you do that?  'buf' as the buffer
// to read bytes from.  
// NOTE: Each number here is of type 'size_t'.  
// NOTE: It looks like the data is completely copied into
// the number. I shouldn't have to worry about data
// disappearing if I destroy the source buffer where the
// characters came from.
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)


// Small utility function for adjusting the output
// of encryption:

size_t makeOutputUniformSize(
		unsigned char * buf, size_t bufLength, size_t desiredLength){

	size_t difference = desiredLength - bufLength;
	if (difference) {
		// The pointer should point to the next location in
		// memory that's writable, so I don't have to adjust it.
		memset(buf, 0, difference);
		//buf += difference; Unless I pass a pointer by
		//reference, there's no point to that.
	}
	return difference;
}

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

// Minimum keyBits is 88 bits, enough bits for more than 10 bytes.
int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);

	// Generate a number.
	// Is it prime? If not, then throw it away make a new one.
	// Otherwise, create a second prime the same way.
	// Once we have both primes, calculate each field of the key.
	// TODO: Check this assumption.
	
	mpz_t p, q;
	mpz_init_set_ui(p, 0); mpz_init_set_ui(q, 0);
	//size_t keyBytes = keyBits / 8;
	size_t factorBytes = keyBits / 16;
	// From use in test function, keyBits refers to length
	// of 'n'. That means that the bytes of the key is
	// 'keyBits / 8', and that each prime factor has to be
	// half that size.
	
	// Size of an unsigned char is 1, just like char.
	// No +1 to size because never printing.
	unsigned char* randBuffer = malloc( sizeof(unsigned char)*factorBytes);

	// For now, let's assume that keybits is a multiple of 16, like the
	// professor recommended.
	// We'll assume that the keybits defines the length of each prime.
	
	while(!ISPRIME(p)){
		randBytes(randBuffer, factorBytes);
		BYTES2Z(p, randBuffer, factorBytes);
	}
	while(!ISPRIME(q)){
		randBytes(randBuffer, factorBytes);
		BYTES2Z(q, randBuffer, factorBytes);
	}
	mpz_set(K->p, p);
	mpz_set(K->q, q);
	mpz_mul(K->n, p, q);

	// phi(n) = phi(pq) = (p-1)(q-1) = pq - p - q + 1 = n - p - q + 1.
	mpz_t phi_n;
	mpz_init_set(phi_n, K->n);
	mpz_sub(phi_n, phi_n, p);
	mpz_sub(phi_n, phi_n, q);
	mpz_add_ui(phi_n, phi_n, 1);

	// This should definitely ensure that our 'e' is smaller than
	// phi(n) without having to perform modular arithmetic.
	size_t bytesForE = factorBytes; // Around as large as a prime.

	NEWZ(d);
	NEWZ(gcd);

	while(1) {
		// Take note of the different length, which is
		// half of what it normally is. This should still
		// be fine since the buffer will have the space for
		// this.
		randBytes(randBuffer, bytesForE);
		BYTES2Z(K->e, randBuffer, bytesForE);

		// Instead of only calculating the gcd to make sure
		// that it's 1, I calculate both the gcd and the
		// multiplicative inverse of e at the same time.
		mpz_gcdext(gcd, d, NULL, K->e, phi_n);

		if (!mpz_cmp_ui(gcd, 1))
			break;
	}
	mpz_mod(K->d, d, phi_n);
	free(randBuffer);
	return 0;
}

// Message format:
// - Let N be the # of bytes for K.n.
//	 - NOTE: # of bytes reported by rsa_numbytesN function is a little
//	    misleading. It'll report number of limbs taken up, so you might
//	    end up getting a size that's much too large. So it's best to
//	    adjust the length of the full plaintext by more than 'N - 1'.
// 
// Plaintext processes in each chunk: N - 10 Bytes
// Total length of final plaintext message: N - 8:
// +---------------------------+--------------------------+
// | Length of actual plaintext|     Plaintext itself     |
// +---------------------------+--------------------------+
// |          2 Bytes          | up to N - 8 - 2 = N - 10 |
// +---------------------------+--------------------------+
//
// Once encrypted, the result will be a maximum of N bytes long. That
// means that for each encrypted chunk, including the residue chunk at
// the end, each one can result in a ciphertext that's N bytes long.
// So to figure out the length that a ciphertext buffer should be, we
// need to calculate the total number of potential chunks, then multiply
// by the full ciphertext length.
//
// NOTE: The result of the ciphertext need not be N bytes long! The
// operation of raising every element to the 'e' power is an injective
// one (it has to be for crypto to even work), and the set that function
// acts on is finite. Therefore raising elements to the 'e' power is a
// bijection. That means that for every element that's less than N bytes
// long normally, there's that many elements that'll be less than N
// bytes long after raising to the 'e' power. It's NOT in general safe
// to assume that the output of the ciphertext will be 'N' bytes long.
// The last byte of N affects the probability that numbers are less than
// 'N' bytes long. If the last byte is '0x01', then almost every number,
// save for N itself is less than N bytes long, and you'll definitely
// have problems.
//
// Full ciphertext message length: <= N + 2 
// +---------------------------+--------------------------+
// |Length of actual ciphertext|     Ciphertext itself    |
// +---------------------------+--------------------------+
// |          2 Bytes          |          up to N         |
// +---------------------------+--------------------------+
// Assuming that 'len' here is the length of each buffer in bytes.
size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{

	// Problem 1: Trailing 0 bytes on our text need to be preserved.
	// Answer: Make note of the size when encrypting and put it in the
	// encrypted message.
	// Problem 2: Resulting ciphertext can be of any size. How do we
	// know how to decrypt an entire string?
	// Answer: Insert the ciphertext lengths right before the encrypted
	// text. Always two bytes. Saying that this is insecure is
	// equivalent to saying that encrypting one single thing is
	// insecure. We're not giving them any additional information about
	// the ciphertext that they wouldn't otherwise have had. We're just
	// noting how to split up a large chunk of ciphertext into smaller
	// chunks.
	
	NEWZ(cipherNum);
	NEWZ(chunk);

	// Making it of type short so that it can neatly be placed within the
	// message itself as two bytes.
	unsigned short chunkSize = RSA_MESSAGE_LEN;
	size_t fullMessageLength = chunkSize + sizeof(chunkSize);
	unsigned short * outputLengthLocation;
	unsigned short outputLength;
	size_t longOutputLength;
	size_t totalOutputLength = 0;
	size_t numChunks = len / chunkSize;
	// Holds the actual message to be encrypted.
	
	// Set up buffer to build final plaintext for encyption.
	unsigned char * finalMessage = malloc(fullMessageLength);
	unsigned char * rsa_message = 
		finalMessage + sizeof(chunkSize);
	unsigned short * rsa_message_len = finalMessage;

	for (size_t i = 0; i < numChunks; i++) {
		// Each ciphertext will start with length of ciphertext as two
		// bytes, plain. Actual ciphertext will start being written two
		// bytes later.
		outputLengthLocation = outBuf;
		outBuf += sizeof(outputLength);

		// Get the first chunk of input.
		*rsa_message_len = chunkSize;
		memcpy(rsa_message, inBuf, chunkSize);
		BYTES2Z(chunk, finalMessage, fullMessageLength);

		// Encrypt it by calculating:
		// chunk^e mod n.
		mpz_powm(cipherNum, chunk, K->e, K->n);

		// Write the cipertext to the output buffer.
		Z2BYTES(outBuf, longOutputLength, cipherNum);
		outputLength = (short) longOutputLength;
		*outputLengthLocation = outputLength;
		inBuf += chunkSize;
		outBuf += outputLength;
		totalOutputLength += sizeof(outputLength) + outputLength;

	}

	// Must handle the final chunk, should there be one:
	unsigned short residue = len % chunkSize;
	if (residue) {
		*rsa_message_len = residue;
		memcpy(rsa_message, inBuf, residue);
		outputLengthLocation = outBuf;
		outBuf += sizeof(residue);

		BYTES2Z(chunk, finalMessage, residue + sizeof(residue) );
		mpz_powm(cipherNum, chunk, K->e, K->n);
		Z2BYTES(outBuf, longOutputLength, cipherNum);
		outputLength = (short)longOutputLength;

		*outputLengthLocation = outputLength;
		totalOutputLength += sizeof(outputLength) + outputLength;
	}
	
	free(finalMessage);

	return totalOutputLength;
}

// Based on the format above, decryption follows these steps:
// 1. Read the first two bytes of the ciphertext. That's the length of
//    the current ciphertext chunk. Call this `curCiperLen`
// 2. Decrypt `curCipherLen` bytes of ciphertext.
// 3. Read the first two bytes of the resulting plaintext. That's the
//    actual length of the plaintext, accounting for trailing zero
//    bytes.
// 4. Check the length of the remaining plaintext (meaning not counting
//    the first two bytes for size). If: 
//    - It is less than the retrieved length, then pad with 0 bytes.
//    - If it is equal, than keep the plaintext as is.                                                        
//    - It is more than the retrieved length, something went wrong.
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{

	NEWZ(chunk);
	NEWZ(plainNum);

	unsigned short cipherLength, plainLength, outLength;
	unsigned short * cipherLengthLoc;
	size_t remaining_len = len;
	size_t decrypted_size;
	size_t totalPlainLength = 0;
	size_t debugLengthCheck = 0;

	while( remaining_len > 0) {
		cipherLengthLoc = inBuf;
		cipherLength = *cipherLengthLoc;
		// Read ciphertext size.
		inBuf += sizeof(cipherLength);
		remaining_len -= sizeof(cipherLength);

		// Read ciphertext.
		BYTES2Z(chunk, inBuf, cipherLength);
		inBuf += cipherLength;
		remaining_len -= cipherLength;

		// Decrypt it by calculating:
		// m^d mod n.
		mpz_powm(plainNum, chunk, K->d, K->n);
		// Get first two bytes of plaintext.
		NEWZ(pLen);
		mpz_mod_ui(pLen, plainNum, 1 << 16);
		//Z2BYTES(&plainLength, debugLengthCheck, pLen);
		plainLength = (unsigned short) mpz_get_ui(pLen);

		//plainLength = *(unsigned short *) plainNum->_mp_d;
		// Divide out 2 Bytes.
		mpz_fdiv_q_2exp(plainNum, plainNum, sizeof(plainLength)*8);

		// Write the plainText to the buffer.
		Z2BYTES(outBuf, decrypted_size, plainNum);
		outBuf += decrypted_size;
		totalPlainLength += decrypted_size;
		if (plainLength > decrypted_size)
			memset(outBuf, 0, plainLength - decrypted_size);
		outBuf += plainLength - decrypted_size;
		// NOTE: May change this to just adding 'plainLength' and skipping
		// the previous addition to totalPlainLength, since it *should* all
		// add up to plainLength;
		totalPlainLength += plainLength - decrypted_size;
	}

	return totalPlainLength;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
