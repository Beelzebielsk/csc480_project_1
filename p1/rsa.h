/* interface for plain RSA.
 * NOTE: this is *INSECURE* for almost any application other
 * than the KEM to which we apply it.  Don't use it alone. */
#pragma once
#include <gmp.h>

#define RSA_MESSAGE_LEN rsa_numBytesN(K) - 10

typedef struct _RSA_KEY {
	mpz_t p;
	mpz_t q;
	mpz_t n;
	mpz_t e;
	mpz_t d;
} RSA_KEY;

/* NOTE: keyBits should be a multiple of 16 to avoid rounding. */
// Eh, not really. keyBits should be a multiple of eight
// bytes, or else using 'rsa_numBytesN' won't really work.
// Basically, you want your 'n' to fully fit into all of
// it's limbs. You don't want the most significant limb to
// contain any zero bytes, or else there's a very big chance
// that a message will get reduced modulo 'n'.
// Keybits must also be large enough that 'n' is at least as large as
// 'rsa_numBytesN' says it is. Every gmp number is stored as a list of
// data 'words', which are either 4 or 8 Bytes in size. In our
// computers, it is likely that it's 8 bytes in size. If we specify
// 'keyBits' that's less than 64 bits in size, then the test will fail
// because of reduction modulo n of the plaintext. The plaintext is
// going to be 8 bytes long, which is larger than n, so information
// will be lost.
int    rsa_keyGen(size_t keyBits, RSA_KEY* K);
/* NOTE: inBuf, when interpreted as a integer, must be less than K->n
 * */
size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len, RSA_KEY* K); 
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len, RSA_KEY* K);
size_t rsa_numBytesN(RSA_KEY* K); 
int rsa_writePublic(FILE* f, RSA_KEY* K);
int rsa_writePrivate(FILE* f, RSA_KEY* K);
int rsa_readPublic(FILE* f, RSA_KEY* K);
int rsa_readPrivate(FILE* f, RSA_KEY* K);
int rsa_initKey(RSA_KEY* K);
int rsa_shredKey(RSA_KEY* K);
