/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>
#include <sys/stat.h> // For stat, finding file length.
#include <sys/mman.h>
#include <gmp.h>
#include <unistd.h> // ftruncate

#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255
#define DEFAULT_KEY_NAME "key"
#define DEFAULT_ENC_NAME "cipher"
#define DEFAULT_DEC_NAME "plain"

enum modes {
	ENC,
	DEC,
	GEN
};

void seedFromFile(const char *);

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */
#define ENTROPY_LEN 64

// Full ciphertext format:
// First row is lengths, second row is contents.
// +----------------------+--------------+-------------------------------------+
// | rsa_numBytesN(K)     | KEM Length   | IV_LEN + input file length + HM_LEN |
// +----------------------+--------------+-------------------------------------+
// | encrypted KEM Length | RSA(x)||H(X) | AESCiphertext                       |
// +----------------------+--------------+-------------------------------------+
int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */

	// Random bytes used for creating key.
	unsigned char x[KLEN_SKE*2];
	randBytes(x, KLEN_SKE*2);
	SKE_KEY AK;
	ske_keyGen(&AK, x, KLEN_SKE*2);
	size_t bytesN = rsa_numBytesN(K);
	size_t rsaMessageLength = RSA_MESSAGE_LEN;

	// Calculates maximum possible length for the KEM. The first two terms
	// calculate the maximum length for the asymmetric key ciphertext
	// length, then we add on the length of the final hash.
	size_t KEM_length =
		( KLEN_SKE*2/ (rsaMessageLength) ) * (bytesN + 2)+ 
		(KLEN_SKE*2 % rsaMessageLength ? bytesN + 2 : 0) +
		HASHLEN
		;
	struct stat inStats;
	stat(fnIn, &inStats);
	size_t maxTotalOutputLength = 
		bytesN + // First fixed length KEM size
		KEM_length + 
		ske_getOutputLen( inStats.st_size ); // Full ciphertext length.


	// Prepare file for output file for writing.
	int outFd = open(fnOut, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
	if (outFd < 0) {
		fprintf(stderr, "Error opening ciphertext output file: %s\n", strerror(errno));
		return -1;
	}
	if (ftruncate(outFd, maxTotalOutputLength)) {
		fprintf(stderr,
				"Error setting length of ciphertext output file: %s\n", strerror(errno));
		return -2;
	}

	unsigned char * outBuf = (unsigned char*)
		mmap(0, maxTotalOutputLength, PROT_READ | PROT_WRITE, MAP_SHARED, outFd, 0);

	unsigned char
		* loc_kem_len = outBuf,
		* loc_kem = outBuf + bytesN,
		* loc_ciphertext;
	
	// Zero pad the length to fix the length at `bytesN`
	memset(loc_kem_len, 0, loc_kem - loc_kem_len);

	KEM_length = rsa_encrypt(loc_kem, x, KLEN_SKE*2, K);
	// Hash all the bytes of the encrypted 'x', then write the
	// output of the hash starting where 'kem_length' ends.
	SHA256(x, KLEN_SKE*2, loc_kem + KEM_length);
	KEM_length += HASHLEN;

	// Ciphertext starts right where capsule ends.
	loc_ciphertext = loc_kem + KEM_length;

	// Encrypt the length of the KEM, then write it in our file.
	mpz_t encryptedKemLen;
	mpz_init_set_ui(encryptedKemLen, KEM_length);
	mpz_powm(encryptedKemLen, encryptedKemLen, K->e, K->n);
	// Write the encrypted length into the buffer. It's already
	// zero-padded by virtue of 
	mpz_export(loc_kem_len, NULL, -1, 1, 0, 0, encryptedKemLen);
	mpz_clear(encryptedKemLen);

	munmap(outBuf, maxTotalOutputLength);
	close(outFd);
	size_t cipherLen = 
		ske_encrypt_file(fnOut, fnIn, &AK, NULL, loc_ciphertext - loc_kem_len);

	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{

	int retVal = 0;
	size_t bytesN = rsa_numBytesN(K);
	/* TODO: write this. */
	/* step 3: derive key from ephemKey and decrypt data. */

	// Step 1: Recover KEM Length:
	int inFd = open(fnIn, O_RDONLY, S_IRWXU);
	if (inFd < 0) {
		fprintf(stderr, "Error opening ciphertext input file: %s\n", strerror(errno));
	}
	struct stat inStats;
	stat(fnIn, &inStats);
	size_t inputFileLength = inStats.st_size;
	unsigned char * inBuf = (unsigned char *)
		mmap(NULL, inputFileLength, PROT_READ, MAP_PRIVATE, inFd, 0);

	unsigned char
		* loc_enc_kem_len = inBuf,
		* loc_kem = inBuf + bytesN,
		* loc_kem_hash,
		* loc_ciphertext;

	mpz_t decryptedKemLength;
	mpz_init(decryptedKemLength);
	mpz_import(decryptedKemLength, bytesN, -1, 1, 0, 0, loc_enc_kem_len);
	mpz_powm(decryptedKemLength, decryptedKemLength, K->d, K->n);
	size_t kem_length = mpz_get_ui(decryptedKemLength);
	// TODO: Include check against kem_length being too long to fit in a
	// size_t variable. If it is, that's a sign of a changed message.
	// Treat it just as if the hash didn't work out.
	mpz_clear(decryptedKemLength);

	loc_ciphertext = loc_kem + kem_length;
	loc_kem_hash = loc_ciphertext - HASHLEN;

	// Step 2: Check to see if symmetric key is valid key.
	
	// NOTE: kem_length includes the length of the hash.
	unsigned char * decrypted_x = malloc(KLEN_SKE*2);
	size_t rsa_output_len = rsa_decrypt(decrypted_x, loc_kem, kem_length - HASHLEN, K);
	unsigned char * xHash = malloc(HASHLEN);
	SHA256(decrypted_x, rsa_output_len, xHash);
	// If they're different, memcmp returns 1 or -1.
	if ( memcmp(xHash, loc_kem_hash, HASHLEN) ) {
		fprintf(stderr, "Integrity of asymmetric key compromised. Exiting.\n");
		retVal = -1;
		goto bad_rsa_hash;
	}

	// Step 3: Recover symmetric key.
	SKE_KEY AK;
	ske_keyGen(&AK, decrypted_x, KLEN_SKE*2);

	// Step 4: Decrypt Ciphertext.
	// Clear file beforehand:
	int outFd = open(fnOut, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
	close(outFd);
	ske_decrypt_file(fnOut, fnIn, &AK, bytesN + kem_length );

	// File has finished decrypting.
end:
bad_rsa_hash:
	close(inFd);
	munmap(inBuf, inputFileLength);
	free(xHash);
	free(decrypted_x);

	return retVal;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */

	seedFromFile(fnRnd);
	RSA_KEY K;
	rsa_initKey(&K);
	int retVal = 0;

	switch (mode) {
		case ENC:
			// Check to make sure all necessary arguments are defined:
			// - key: You need someone else's public key to encrypt. Using
			//      your own makes no sense, unless you're sending this to
			//      yourself. Even if you are, you can still generate the key
			//      beforehand.
			// - input file: What are you encrypting?

			if (!*fnKey) {
				fprintf(stderr, "Must specify key file using '-k'.\n");
				retVal = -1;
				//return -1;
				goto end;
			} else if (!*fnIn) {
				fprintf(stderr, "Must specify output file using '-i'.\n");
				retVal = -1;
				//return -1;
				goto end;
			} else if (!*fnOut) {
				strncpy(fnIn, DEFAULT_ENC_NAME, FNLEN);
			}
			{
				FILE * keyFile = fopen(fnKey, "rb");
				rsa_readPublic(keyFile, &K);
				fclose(keyFile);
			}
      kem_encrypt(fnOut, fnIn, &K);
			break;
		case DEC:
			if (!*fnKey) {
				fprintf(stderr, "Must specify key file using '-k'.\n");
				retVal = -1;
				//return -1;
				goto end;
			} else if (!*fnIn) {
				fprintf(stderr, "Must specify output file using '-i'.\n");
				retVal = -1;
				//return -1;
				goto end;
			} else if (!*fnOut) {
				strncpy(fnIn, DEFAULT_DEC_NAME, FNLEN);
			}
			{
				FILE * keyFile = fopen(fnKey, "rb");
				rsa_readPrivate(keyFile, &K);
				fclose(keyFile);
				if( mpz_cmp_ui(K.d, 0) == 0 ) {
					fprintf(stderr, "Provided public key when private key needed.\n");
					retVal = -2;
					goto end;
					//return -2;
				}
			}
			kem_decrypt(fnOut, fnIn, &K);
			break;
		case GEN:
			// Generate entropy from file:
			//seedFromFile(fnRnd);
			rsa_keyGen(nBits, &K);
			size_t fullNameLength = FNLEN + 5;
			// + 5 for '.pub' and null character.
			size_t actualNameLength;
			char * fullName = malloc(fullNameLength);

			// Create public file name.
			actualNameLength = strlen(fnOut);
			memcpy(fullName, fnOut, actualNameLength);
			// '.pub' is four characters, plus 5 for a terminating NUL.
			memcpy(fullName + actualNameLength, ".pub", 5);
			actualNameLength += 5;

			FILE * pubKeyFile = fopen(fullName, "wb");
			rsa_writePublic(pubKeyFile, &K);
			fclose(pubKeyFile);

			// The actual argument is equiavlent to the private file name.
			FILE * priKeyFile = fopen(fnOut, "wb");
			rsa_writePrivate(priKeyFile, &K);
			fclose(priKeyFile);

			free(fullName);
			break;
		default:
			return 1;
	}

end:
	rsa_shredKey(&K);
	return retVal;
}

void seedFromFile(const char * filename) {
	FILE* entFile = fopen(filename, "rb");
	unsigned char * entropy = malloc(ENTROPY_LEN);
	fread(entropy, 1, ENTROPY_LEN, entFile);
	setSeed(entropy, ENTROPY_LEN);
	free(entropy);
}
