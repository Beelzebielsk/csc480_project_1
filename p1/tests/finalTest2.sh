#!/bin/bash

# What does this do:
# Tests the ability to transmit information between two distinct parties
# using the kem-enc program. Does the following:
# 1. Carries on an imaginary conversation between two parties, based on
#    the names in a conversation file named 'fullConvo.txt'. 
# 2. Then, the conversation is recreated from each party's point of
#    view, where messages from the other party are encrypted, then
#    decrypted, and the party's own messages are included plainly.
# 3. The conversations are then reconstructed and compared both to each
#    other party's conversation and the original conversation. If
#    everything's equivalent, then the conversation was carried out
#    successfully. 

# What you need to run this:
# 1. Place this in your 'tests' folder for the project.
# 2. Create a folder named './finalTestData' in your tests directory.
#    You can change the name if you wish, but you must change a variable
#    in the 'Preliminaries' section of the script to do so.
# 3. Create a text file named 'fullConvo.txt' with a conversation in
#    './finalTestData' and populate it with lines of a conversation.
#    Each line must start with a name, followed by a colon.

# Full Convo Rules:
# 1. The conversation consists of lines that start with the name of the
#    message sender, followed by a colon (ex. 'Alice:'). Case is
#    *insignificant*.
# 2. There can be a maximum of two different parties.
# 3. One party can transmit as many messages as they want consecutively.

# Necessary Programs:
# 1. Completed ./kem-enc
# 2. 'split' which splits files into chunks
# 3. 'sed' which edits a text stream based upon given commands.
# 4. 'diff' which shows differences between files.

# To alter the locations for files that the script uses, just alter the
# parameters listed in this section. There's little need to alter
# anything further down the script.

# Debug Levels:
# 1. Print variable names.
# 2. Debug C program calls for encryption and decryption.

# Programatically access a variable.
toVariable() {
	eval echo '$'$1
}

############################################################
## Preliminary Setup: Information about each party {{{
############################################################

DEBUG=${DEBUG:-0}
if [ $DEBUG -gt 0 ]; then
	echo "DEBUG: ${DEBUG}"
fi;

# Specifies location of your crypto program.
crypto_suite="../kem-enc"

# Controls where conversations are stored.
testDataDir="./finalTestData"

originalConvo="${testDataDir}/fullConvo.txt"

# To alter the party names:
# 1. Change the names in the 'party1' and 'party2' variables.
# 2. Make sure to make variables for each name which equal the party
#    number for each name.
#    EX:
#			party1="funnyMan"
#			party2="sadMan"
#			funnyMan=1
#			sadMan=2
#party1="alice"
#party2="bob"
#alice=1
#bob=2

names=( $(cat $originalConvo | sed -n -e "s/^\([[:alpha:]]\+\):.*/\1/ip" | sort | uniq ) )

party1=${names[0]}
party1=${party1,,}
party2=${names[1]}
party2=${party2,,}

eval $party1=1
eval $party2=2

if [ $DEBUG -gt 0 ]; then
	echo "Names List: ${names[*]}"

	echo "Party1: $party1"
	echo "Party2: $party2"

	echo "named variable for party 1: $(toVariable $party1)"
	echo "named variable for party 2: $(toVariable $party2)"
fi;

# Controls where party-specific information is stored, such as:
# - Conversation message lists.
# - RSA keys.
party1Dir="${testDataDir}/${party1}"
party2Dir="${testDataDir}/${party2}"

# The location of the conversation itself.

party1ConvoDir="${party1Dir}/convo"
party2ConvoDir="${party2Dir}/convo"

############################################################
## }}}
############################################################

############################################################
## Creating message list to order transmissions : {{{
############################################################

# If any of the test directories already existed, delete and recreate
# them.

messagesDir="${testDataDir}/messageList"
rm -rf ${party1Dir} ${party2Dir} ${messagesDir}

mkdir -p "${party1ConvoDir}" 
mkdir -p "${party2ConvoDir}" 
mkdir -p "${messagesDir}" 

#echo $party1ConvoDir
#echo $party2ConvoDir

split -del1 $originalConvo $messagesDir/message_

############################################################
## }}}
############################################################

# Generate a key for each party.

$crypto_suite -b 2048 -g ${party1Dir}/key
$crypto_suite -b 2048 -g ${party2Dir}/key

# Trade keys

cp ${party2Dir}/key.pub ${party1Dir}/otherKey.pub
cp ${party1Dir}/key.pub ${party2Dir}/otherKey.pub

############################################################
## Transmission Functions: {{{
############################################################

# 1: Destination party number.
# 2: Source party number.
# 3. Message to transmit.
# Copies message to convo folder of sender.
transmitMessage() {
	local p1=$1 # Destination Party Number
	local p2=$2 # Source Party Number
	local message=$3 # File name of Message
	local dst="party$p1" # Destination Party for message
	local src="party$p2" # Source party for message
	local encMsg="$(toVariable ${dst}Dir)/encMsg"
	local basename=${message##*/}
	local decMsg=$(toVariable ${dst}ConvoDir/${basename})
	local pubKey="$(toVariable ${src}Dir/otherKey.pub)"
	local priKey="$(toVariable ${dst}Dir/key)"

	if [ $DEBUG -gt 0 ]; then
		echo "Destination Directory: $(toVariable ${dst}Dir)"
		echo "Source Directory: $(toVariable ${src}Dir)"
		echo "Encrypted Message: $encMsg"
		echo "Base name of message: $basename"
		echo "Decrypted Message: $decMsg"
		echo "Public Key File: $pubKey"
		echo "Private Key File: $priKey"
		echo ""
	fi;

	# Transmit encrypted message.
	$crypto_suite -k $pubKey -i $message -o $encMsg -e
	$crypto_suite -k $priKey -i $encMsg -o $decMsg -d

	if [ $DEBUG -gt 1 ]; then
		gdb $crypto_suite -ex "set args -k $pubKey -i $message -o $encMsg -e"
		gdb $crypto_suite -ex "set args -k $priKey -i $encMsg -o $decMsg -d"
	fi

}

############################################################
## }}}
############################################################

############################################################
## Transmissions : {{{
############################################################

for message in $messagesDir/*; do
	speaker=$(cat $message | sed -n -e "s/^\([[:alpha:]]\+\):.*/\1/ip") 
	speaker=${speaker,,}
	speaker=$(toVariable $speaker)
	receiver=$([ $speaker -eq 1 ] && echo 2 || echo 1)
	speakerParty="party$speaker"
	receiverParty="party$speaker"

	basename=${message##*/}
	srcMsgDir="$(toVariable ${speakerParty}ConvoDir)"
	dstMsgDir="$(toVariable ${receiverParty}ConvoDir)"

	# Send Message: Copy to sender's folder plain, send to receiver's
	# folder encrypted, then have receiver decrypt.
	cp $message $srcMsgDir/$basename
	transmitMessage $receiver $speaker $message
done;


############################################################
## }}}
############################################################

############################################################
## Tests: {{{
############################################################

# Recreate full conversations for each party:

party1Convo="${party1ConvoDir}/convo.txt"
party2Convo="${party2ConvoDir}/convo.txt"

touch ${party1Convo}
for message in ${party1ConvoDir}/*; do
	cat $message >> ${party1Convo}
done;

touch ${party2Convo}
for message in ${party2ConvoDir}/*; do
	cat $message >> ${party2Convo}
done;

# Test 1: Did both parties have same conversation?

echo "Test 1: Check if both parties' conversations were the same."
diff -u ${party1Convo} ${party2Convo}
testResults[0]=$?
echo "Test 1:"  $([[ ${testResults[0]} -eq 0 ]] && echo "passed" || echo "failed")

# Tests 2 and 3: Did they both have the *original* conversation?

echo "Test 2: Check if party 1's conversation was the same as original."
diff -u ${party1Convo} ${testDataDir}/fullConvo.txt
testResults[1]=$?
echo "Test 2:"  $([[ ${testResults[1]} -eq 0 ]] && echo "passed" || echo "failed")

echo "Test 3: Check if party 2's conversation was the same as original."
diff -u ${party2Convo}  ${testDataDir}/fullConvo.txt
testResults[2]=$?
echo "Test 3:"  $([[ ${testResults[2]} -eq 0 ]] && echo "passed" || echo "failed")

############################################################
## }}}
############################################################
