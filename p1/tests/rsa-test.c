/* test code for RSA */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../rsa.h"
#include "../prf.h"

/* turn this on to print more stuff. */
#define VDEBUG 0
#define KEYDEBUG 0
/* turn this on for randomized tests. */
#define RANDKEY 0

#define SMALLPRIMES 5

#define NEWZ(x) mpz_t x; mpz_init(x)
#define DEFZ(x,setter) mpz_t x; mpz_init_set(x, setter)

/* encrypt / decrypt some strings, and make sure
 * this composition is the identity */

int keyTest();
void failParse(int failures);
void printBinaryString(unsigned char * binString, size_t len);

int main() {
	fprintf(stderr, "testing rsa...\n");
	char *pass,*fail;
	if (isatty(fileno(stdout))) {
		pass = "\033[32mpassed\033[0m";
		fail = "\033[31mfailed\033[0m";
	} else {
		pass = "passed";
		fail = "failed";
	}
#if RANDKEY
	setSeed(0,0);
#else
	setSeed((unsigned char*)"random data :D:D:D",18);
#endif
	RSA_KEY K;
	size_t keyLength = 1024;
	rsa_keyGen(keyLength,&K);

	int keyFail = 0;
	if( KEYDEBUG ) keyFail = keyTest(&K);
	if( KEYDEBUG && keyFail ){
		failParse(keyFail);
		return keyFail;
	}

	// Testing the key itself to make sure it works.
	size_t i,j,ctLen,mLen = rsa_numBytesN(&K);
	size_t ctMultiplier = 3;
	unsigned char* pt = malloc(mLen);
	unsigned char* ct = malloc(mLen*ctMultiplier);
	// Times 2 for just in case there's a whole second block.
	unsigned char* dt = malloc(mLen);
	int rsaTestFail = 0;
	for (i = 0; i < 10; i++) {
		pt[mLen-1] = 0; /* avoid reduction mod n. */
		randBytes(pt,mLen-1);
		/* encrypt, decrypt, check. */
		ctLen = rsa_encrypt(ct,pt,mLen,&K);
#if VDEBUG
		printf("Length of the plainText: %lu\n", mLen);
		printf("Length of the cipherText: %lu\n", ctLen);
#endif
		rsa_decrypt(dt,ct,ctLen,&K);
		for (j = 0; j < mLen; j++) {
			if (dt[j] != pt[j]) break;
		}
		//rsaTestFail |= ( (j==mLen) ? 0 : 1 );
		// Taking away the or, because some key lenghts have
		// partial failures (not all strings fail), and I'd like
		// to investigate those.
		rsaTestFail += ( (j==mLen) ? 0 : 1 );
		fprintf(stderr, "test[%02lu] %s\n",i, !rsaTestFail?pass:fail);
		if (j!=mLen){
			fprintf(stderr, "Plaintext: ");
			printBinaryString(pt, mLen);

			fprintf(stderr, "Ciphertext: ");
			printBinaryString(ct, mLen);

			fprintf(stderr, "Decrypted Text: ");
			printBinaryString(dt, mLen);

		}
	}
	free(pt); free(ct); free(dt);
	return rsaTestFail;
}

int keyTest(RSA_KEY *K) {
	// The bits of the return value indicate
	// a test that's been failed.
	// n = pq: 1st bit (1)
	// ed mod phi_n = 1: 2nd bit (2)
	// r^phi_n mod n = r: 3rd bit (4)
	// m^{ed} = m 4th bit (8)
	
	int testsFailed = 0;
	
	// Define the pass and fail strings.
	char *pass,*fail;
	if (isatty(fileno(stdout))) {
		pass = "\033[32mpassed\033[0m";
		fail = "\033[31mfailed\033[0m";
	} else {
		pass = "passed";
		fail = "failed";
	}

	gmp_printf("p: %Zd\nq: %Zd\nn: %Zd\ne: %Zd\nd: %Zd\n",
			K->p, K->q, K->n, K->e, K->d);
	puts("Key Tests:");
	
	// Create some primes for testing phi_n.
	mpz_t smallPrimes[SMALLPRIMES];
	mpz_init_set_ui(smallPrimes[0], 2);
	mpz_init_set_ui(smallPrimes[1], 3);
	mpz_init_set_ui(smallPrimes[2], 5);
	mpz_init_set_ui(smallPrimes[3], 7);
	mpz_init_set_ui(smallPrimes[4], 11);

	// Calculate phi_n.
	//DEFZ(phi_n, K->n);
	//mpz_sub(phi_n, phi_n, K->p);
	//mpz_sub(phi_n, phi_n, K->q);
	//mpz_sub_ui(phi_n, phi_n, 1);
	NEWZ(phi_n);
	NEWZ(phi_q);
	mpz_sub_ui(phi_n, K->p, 1);
	mpz_sub_ui(phi_q, K->q, 1);
	mpz_mul(phi_n, phi_n, phi_q);

	NEWZ(difference);
	mpz_set(difference, K->n);
	mpz_submul(difference, K->p, K->q);

	// Test for n = pq:
	int productTest = 1;
	if (!mpz_cmp_ui(difference, 0)) {
		gmp_fprintf(stderr, "n = pq: %s\n", pass);
	} else {
		testsFailed |= productTest;
		gmp_fprintf(stderr, "n = pq: %s\nResult: %Zd", fail, difference);
	}

	//Test for e and d correctness. Tests for 'ed mod phi_n = 1'.
	int inverseTest = 2;
	NEWZ(inverse_calc);
	mpz_mul(inverse_calc, K->e, K->d);
	mpz_mod(inverse_calc, inverse_calc, phi_n);
	//puts("Inverse Calculated:"); // VERYDEBUG
	if (!mpz_cmp_ui(inverse_calc, 1)) {
		gmp_fprintf(stderr, "ed mod phi_n = 1: %s\n", pass);
	} else {
		testsFailed |= inverseTest;
		gmp_fprintf(stderr, "ed mod phi_n = 1: %s\nResult: %Zd", fail, inverse_calc);
	}

	//Test for phi_n correctness: Will raising to the phi_n yield 1 for
	//relatively prime numbers?
	int powerTest = 4;
	NEWZ(raised);
	//puts("Initiated 'raised':"); //VERYDEBUG
	for(size_t i = 0; i < SMALLPRIMES; i++) {

		int primeEqual = 0;
		primeEqual |= ( mpz_cmp(smallPrimes[i], K->p) ? 1 : 0);
		primeEqual |= ( mpz_cmp(smallPrimes[i], K->q) ? 1 : 0);
		if (primeEqual) continue;

		mpz_set(raised, smallPrimes[i]);
		//puts("Gave 'raised' a value."); //VERYDEBUG
		mpz_powm(raised, raised, phi_n, K->n);
		//puts("Raised 'raised'"); //VERYDEBUG
		if (!mpz_cmp_ui(raised, 1)) {
			gmp_fprintf(stderr,
					"%Zd^phi_n mod n = 1: %s\n", smallPrimes[i], pass);
		} else {
		testsFailed |= powerTest;
			gmp_fprintf(stderr, 
					"%Zd^phi_n mod n = 1: %s\nResult: %Zd\n",
					smallPrimes[i], fail, raised);
		}
	}

	//Test for encryption of very small messages. Will test on arbitrary
	//numbers.
	int encryptionTest = 8;
	NEWZ(encrypted);
	NEWZ(decrypted);
	for(size_t i = 0; i < SMALLPRIMES; i++) {
		mpz_powm(encrypted, smallPrimes[i], K->e, K->n);
		mpz_powm(decrypted, encrypted, K->d, K->n);
		if (!mpz_cmp(smallPrimes[i], decrypted)){
			gmp_fprintf(stderr, "%Zd^{ed} mod n = %Zd: %s\n",
					smallPrimes[i], smallPrimes[i], pass);
		} else {
		testsFailed |= encryptionTest;
			gmp_fprintf(stderr, "%Zd^{ed} mod n = %Zd: %s\nResult: %Zd\n",
					smallPrimes[i], smallPrimes[i], fail, decrypted);
		}
	}

	return testsFailed;
	// Test for phi_n correctness:
	//gmp_printf("n = pq: %Zd\ne and d inverses: %Zd\n11^e: %Zd\n11^ed: %Zd\n", difference, inverse_test, encrypted, decrypted);
}

void failParse(int failures){
	
	// Define the pass and fail strings.
	char *pass,*fail;
	if (isatty(fileno(stdout))) {
		pass = "\033[32mpassed\033[0m";
		fail = "\033[31mfailed\033[0m";
	} else {
		pass = "passed";
		fail = "failed";
	}

	int testNum = 1;
	char * nStatus,
			 * inverseKeysStatus,
			 * phi_n_status,
			 * enc_dec_status;
	
	nStatus = ( failures & testNum ? fail : pass );
		testNum = testNum << 1;
	inverseKeysStatus = ( failures & testNum ? fail : pass );
		testNum = testNum << 1;
	phi_n_status = ( failures & testNum ? fail : pass );
		testNum = testNum << 1;
	enc_dec_status = ( failures & testNum ? fail : pass );
	printf("Malformed key. Failed:\n\
			correct calculation of n: %s\n\
			correct calculation of d: %s\n\
			phi_n works correctly: %s\n\
			encrypt and decrypt: %s\n\
			Exiting Test.\n",
			nStatus, inverseKeysStatus,
			phi_n_status, enc_dec_status);
}

void printBinaryString(unsigned char * binString, size_t len){
	for (size_t i = 0; i < len; i++){
		fprintf(stderr, "%02x",binString[i]);
	} fprintf(stderr, "\n");
}
