# Some keylengths for the rsa_keygen work under strange conditions.
# This script will run through various keyLengths and conditions, then
# record the conditions under which they fail.

############################################################
## Set-up stuff: {{{
############################################################

# List of current conditions:
# - keyLength too small.
# - printing during rsa_keygen file.
# - size of cipherText buffer relative to plaintext buffer. Ciphertext
#   buffer has to be large to a certain degree.

# Currently, just going to test the printing conditions. Will use
# three arrays:
# - Keys that always fail.
# - Keys that fail only when printing.
# - Keys that always succeed.
# set formatlistpat=^\\s*\\(\\d\\+[\\]:.)}\\t\ ]\\\\|-\\)\\s*
#completeFail = [];
#printFail = []
#completeSuccess = [];
#temp_failures = [];

# For now, start by 8 bytes and step by 8 bytes, since these
# are the increments that gmp integers increase in total size.
#keyLengths = range(64, 1024+1, 64);
keyLengths = range(320, 1024+1, 64);

# Find line where keyGenFunction occurs.

breakpoints = {
    'rsa_keyGenLine' : 46,
    'main_returnLine' : 89,
    'rsa_debugPrintsLine' : 'rsa.c:91',
}

keyLengthVarName = 'keyLength';

############################################################
## }}}
############################################################

############################################################
## GDB Quick Functions {{{
############################################################

def gdb_break(breakPoint):
    gdb.execute("break {0}".format(breakPoint) );

def gdb_getValue(name):
    gdb.execute("print {0}".format(name) );
    return gdb.history(0);

def gdb_setValue(name, value):
    gdb.execute("set variable {0} = {1}".format(name, value) );
    actual = gdb_getValue(name)
    if actual != value:
        print("Unable to set variable '{0}' to value '{1}'"
                .format(name, value) );
        raise Exception();
        #gdb.execute("stop");
        #gdb.execute("quit");

# - Failure: Failed at least once.
# - Consistent: Results always the same.
# - fullFail: Results always the same AND always 'maxFailRate', which
#   is currently 10.
def fullTest(klen, timesRun=1, debugPrints=1):
    testResults = {
        'klen'        : klen,
        'debugPrints' : debugPrints,
        'results'     : []
    };
    for time in range(timesRun):
        gdb.execute("start rsa-test");
        gdb.execute("echo Current Key Length: {0}\\n".format(klen));
        gdb.execute("continue"); #keyGen breakpoint.
        gdb_setValue(keyLengthVarName, klen);

        gdb.execute("continue"); #debugPrint breakpoint.
        gdb_setValue("debugPrints", debugPrints);

        gdb.execute("continue"); #main return breakpoint.
        rsaFail = gdb_getValue("rsaTestFail");
        testResults['results'].append(rsaFail);
        gdb.execute("continue"); #Finish execution.
    maxFailRate = 10;
    tests = [
            lambda i: i > 0, # Greater than 0 implies failed test.
            lambda i: i == testResults['results'][0], # Check if all same.
            lambda i: i == maxFailRate, # Check if all tests failed every time.
        ];

    tests = zip(tests, [testResults['results']]*len(tests));

    failure = any( map( *next(tests) ) );
        #any( map( lambda i: i > 0, testResults['results'] ) );
    consistent = all(map( *next(tests) ) );
        #all( map( lambda i: i == testResults['results'][0], testResults['results'] ) );
    fullFail = all( map( *next(tests) ) );
        #all( map(lambda i: i == maxFailRate, testResults['results']) );
    testResults.update( 
        {   
            'failure': failure,
            'consistent' : consistent,
            'fullFail' : fullFail 
        } );
    return testResults;

def resultSummary(pr, nopr):
    if pr['fullFail'] and nopr['fullFail']:
        print("Fails all times under both conditions.");
    elif pr['failure'] and pr['consistent'] and nopr['failure'] and nopr['consistent']:
        if pr['results'][0] == nopr['results'][0]:
            print("Fails same number of times under both conditions.")
        else:
            print("Failure rate different between conditions,",
                "but constant rate for each condition.");
    elif not pr['failure'] and not nopr['failure']:
        print("Succeeds all times under both conditions.");
    else:
        printStatusString = "Succeeds" if not pr['failure'] else "Fails";
        noPrintStatusString = "Succeeds" if not nopr['failure'] else "Fails";
        printConsistencyString = "Consistently" if pr['consistent'] else "Inconsistently";
        noPrintConsistencyString = "Consistently" if nopr['consistent'] else "Inconsistently";
        print("{0} {1} when printing, {2} {3} when not printing."
                .format(printStatusString, printConsistencyString,
                noPrintStatusString, noPrintConsistencyString) );

############################################################
## }}}
############################################################


# TODO: Find actual line

gdb.execute("file rsa-test");
for point in breakpoints.values():
    gdb_break(point);

printResults = []
nonPrintResults = []
for klen in keyLengths:
    resultPrint = fullTest(klen, timesRun=10);
    resultNoPrint = fullTest(klen, debugPrints=0, timesRun=10);
    printResults.append(resultPrint);
    nonPrintResults.append(resultNoPrint);
    #print(resultPrint);
    #print(resultNoPrint);

print("Final Results:");

for test in range(len(printResults)):
    currentPrint = printResults[test];
    currentNoPrint = nonPrintResults[test];
    print("Key Length: ", currentPrint['klen']);
    print("\tFailure Results of printing: ",
        repr( list( map( lambda v: str(v), currentPrint['results'] )) ));
    print("\tFailure Results of not printing: ",
        repr( list( map( lambda v: str(v), currentNoPrint['results'] )) ));
    resultSummary(currentPrint, currentNoPrint);

gdb.execute("quit");
gdb.execute("y");
