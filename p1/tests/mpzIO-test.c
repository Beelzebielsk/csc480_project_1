#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../rsa.h"
#include "../prf.h"

/* turn this on to print more stuff. */
#define VDEBUG 1
#define KEYDEBUG 1

#define SMALLPRIMES 5

#define NEWZ(x) mpz_t x; mpz_init(x)
#define DEFZ(x,setter) mpz_t x; mpz_init_set(x, setter)
#define DEFZ_ui(x,setter) mpz_t x; mpz_init_set_ui(x, setter)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)
#define ZLENGTH(x) sizeof(mp_limb_t)*mpz_size(x)

int keyTest();
void failParse(int failures);
void printBinaryString(unsigned char * binString, size_t len);

int main() {
	char *pass,*fail;
	if (isatty(fileno(stdout))) {
		pass = "\033[32mpassed\033[0m";
		fail = "\033[31mfailed\033[0m";
	} else {
		pass = "passed";
		fail = "failed";
	}

	setSeed((unsigned char*)"random data :D:D:D",18);
	RSA_KEY K;
	rsa_keyGen(1024,&K);
	RSA_KEY * kptr = &K;
	fprintf(stderr, "Size of a limb: %lu\n", sizeof(mp_limb_t));
	fprintf(stderr, "Size of n: %lu\n", rsa_numBytesN(&K));

	NEWZ(in);
	NEWZ(out);
	size_t randLen = 4;
	size_t outLen;
	unsigned char * randBuf = malloc(sizeof(unsigned char)*4);
	randBytes(randBuf, randLen);
	BYTES2Z(in, randBuf, randLen);

	printf("# Bytes written to 'in': %lu\n", randLen);
	printf("Size of 'in': %lu\n", sizeof(mp_limb_t)*mpz_size(in));

	mpz_sub_ui(out, in, 1);

	printf("Size of 'out' after %s: %lu\n",
			"subtraction",
			sizeof(mp_limb_t)*mpz_size(out));

	mpz_mod(out, in, K.n);

	printf("Size of 'out' after %s: %lu\n",
			"mod n",
			sizeof(mp_limb_t)*mpz_size(out));

	mpz_powm(out, in, K.e, K.n);

	printf("Size of 'out' after %s: %lu\n",
			"in^e mod n",
			sizeof(mp_limb_t)*mpz_size(out));

	mpz_powm(out, out, K.d, K.n);

	printf("Size of 'out' after %s: %lu\n",
			"in^e^d mod n",
			sizeof(mp_limb_t)*mpz_size(out));

	/*
	DEFZ_ui(power, 5);
	printf("Number of bytes in 'n': %lu\n", rsa_numBytesN(&K));
	for (size_t i = 0; i < 50000000; i++){
		mpz_powm(out, out, power, K.n);
		mpz_add_ui(power, power, 5);
		size_t outSize = sizeof(mp_limb_t)*mpz_size(out);
		if (outSize != rsa_numBytesN(&K)) {
		gmp_printf("Size of 'out' after %s: %lu\nPower: %Zd",
				"squaring",
				outSize,
				power);
		}
	}
	*/

	// Testing out how many bytes are used in reading and
	// writing now.
	
	unsigned char * outBuf = malloc(sizeof(unsigned char)*128);
	size_t bytesUsed;
	Z2BYTES(outBuf, bytesUsed, in);

	printf("Bytes used to write randNum: %lu\n", bytesUsed);
	printf("Byte length of randNum: %lu\n", ZLENGTH(in));

	mpz_powm(out, in, K.e, K.n);
	Z2BYTES(outBuf, bytesUsed, out);

	printf("Number of bytes used to write in^e mod n: %lu\n", bytesUsed);
	printf("Byte length of in^e mod n itself: %lu\n", ZLENGTH(out));
	printf("Byte length of in: %lu\n", ZLENGTH(in));

	mpz_powm(out, out, K.d, K.n);
	Z2BYTES(outBuf, bytesUsed, out);

	printf("Number of bytes used to write in^{ed} mod n: %lu\n", bytesUsed);
	printf("Byte length of in^{ed} mod n itself: %lu\n", ZLENGTH(out));
	printf("Byte length of in: %lu\n", ZLENGTH(in));

	// Number of bytes in an mpz number is flexible, it seems.
	// It is as large as it needs to be in limbs. But will always be a
	// multiple of 8 bytes since it's in limbs.
	
	puts("Checking to see how gmp creates numbers from bytes:");
	unsigned char bytes[2] = {1, 2};
	NEWZ(byteTest);
	BYTES2Z(byteTest, bytes, 2);
	gmp_printf("Value of byte test: %Zd\n", byteTest);

	// Seems that it reads in bytes much like how it stores them: In
	// little endian. So the first byte will be the least signifcant
	// byte, and each next byte afterward is the next highest byte.
	// So the two bytes 0x0001 become the value 256:
	//	0*256^0 + 1*256^1 = 256
	// And the two bytes 0x0002 become the value 512:
	//	0*256^0 + 2*256^1 = 512
	// And the two bytes 0x1002 become the value 513:
	free(randBuf);
	return 0;
}
