#!/bin/bash

# Tests the ability to transmit information between two distinct parties
# using the kem-enc program. Does the following:
# 1. Creates two imaginary parties named 'Alice' and 'Bob', who carry a
#    conversation with each other, as described in the 'fullConvo.txt'
#    file. Currently, for the script to work, no party can speak twice
#    in a row, or more.
# 2. Then, the conversation is recreated from each party's point of
#    view, where messages from the other party are encrypted, then
#    decrypted, and the party's own messages are included plainly.
# 3. The conversations are then reconstructed and compared both to each
#    other party's conversation and the original conversation. If
#    everything's equivalent, then the conversation was carried out
#    successfully. 

############################################################
## Preliminary Setup: Information about each party {{{
############################################################

party1="alice"
party2="bob"

# Controls where conversations are stored.
testDataDir="./finalTestData"

# Controls where party-specific information is stored, such as:
# - Conversation message lists.
# - RSA keys.
party1Dir="${testDataDir}/${party1}"
party2Dir="${testDataDir}/${party2}"

# Helps with ordering. *Don't touch this*.
firstParty=$(sed -n -e "1 s/^\([[:alpha:]]\+\).*/\1/ip" < ${testDataDir}/fullConvo.txt)
firstParty=${firstParty,,}
echo $firstParty #DEBUG

# Specifies location of your crypto program.
crypto_suite="../kem-enc"

############################################################
## }}}
############################################################

rm -rf ${party1Dir}
rm -rf ${party2Dir}
mkdir -p ${party1Dir}/convo
mkdir -p ${party2Dir}/convo

# Create the conversation for each party:

party1convo=${party1Dir}/${party1}_side.txt
party2convo=${party2Dir}/${party2}_side.txt

touch ${party1convo}
touch ${party2convo}
cat ${testDataDir}/fullConvo.txt | sed -n -e "s/^${party1}:/&/ip" >> ${party1convo}
cat ${testDataDir}/fullConvo.txt | sed -n -e "s/^${party2}:/&/ip" >> ${party2convo}

# Splits each conversation side into separate files, each containing a
# single line of that party's side of the conversation.
split -del1 ${party1convo} ${party1Dir}/convo/message_
split -del1 ${party2convo} ${party2Dir}/convo/message_

# Generate a key for each party.

$crypto_suite -b 2048 -g ${party1Dir}/key
$crypto_suite -b 2048 -g ${party2Dir}/key

# Trade keys

cp ${party2Dir}/key.pub ${party1Dir}/otherKey.pub
cp ${party1Dir}/key.pub ${party2Dir}/otherKey.pub

party1MessageList=($(echo ${party1Dir}/convo/message*) );
party2MessageList=($(echo ${party2Dir}/convo/message*) );

# Helps ensure the correct order of messages.

# Placing the 'a' at the end of the file is a hack to make sure that the
# files for individual messages are placed in the correct order.
# Lexicographically, 'message_xxa' comes after 'message_xx', so if
# party1 spoke second, then their messages should have the 'a' appended,
# because party1's 'message_00' would be the second message in the
# conversation. party2's 'message_00' would now be the first.  This
# result is hackily acheived by making appending the 'a' onto the names
# of all message files for the party that speaks second.  I readily
# admit this is a bullshit approach, but I'm just throwing this together
# in some spare time.

if [[ $party1 != $firstParty ]]; then
	#echo "$party1 did not go first".
	for message in ${party1MessageList[*]}; do
		mv ${message} ${party1Dir}/convo/${message##*/}a
	done;
	party1MessageList=($(echo ${party1Dir}/convo/message*) );
fi

if [[ $party2 != $firstParty ]]; then
	#echo "$party2 did not go first".
	for message in ${party2MessageList[*]}; do
		mv ${message} ${party2Dir}/convo/${message##*/}a
	done;
	party2MessageList=($(echo ${party2Dir}/convo/message*) );
fi

############################################################
## Party 1 receiving party 2's messages. {{{
############################################################

for message in ${party2MessageList[*]}; do
	curMsg="${party1Dir}/curMsg"
	basename="${message##*/}"
	afterMsg="${party1Dir}/convo/${basename}"
	$crypto_suite -k ${party2Dir}/otherKey.pub -i $message -o $curMsg -e
	$crypto_suite -k ${party1Dir}/key -i $curMsg -o $afterMsg -d
done;

touch ${party1Dir}/convo/convo.txt;

for message in ${party1Dir}/convo/message*; do
	cat $message >> ${party1Dir}/convo/convo.txt;
done;

############################################################
## }}}
############################################################

############################################################
## Party 2 receiving party 1's messages. {{{
############################################################

for message in ${party1MessageList[*]}; do
	curMsg="${party2Dir}/curMsg"
	basename="${message##*/}"
	afterMsg="${party2Dir}/convo/${basename}"
	#echo "Message name: $afterMsg"
	$crypto_suite -k ${party1Dir}/otherKey.pub -i $message -o $curMsg -e
	$crypto_suite -k ${party2Dir}/key -i $curMsg -o $afterMsg -d
done;

touch ${party2Dir}/convo/convo.txt;

for message in ${party2Dir}/convo/message*; do
	cat $message >> ${party2Dir}/convo/convo.txt;
done;

############################################################
## }}}
############################################################

############################################################
## Tests: {{{
############################################################

# test1: Did they both have the same conversation?

declare -a testResults
numTests=3

echo "Test 1: Check if both parties' conversations were the same."
diff -u ${party1Dir}/convo/convo.txt ${party2Dir}/convo/convo.txt
testResults[0]=$?
echo "Test 1:"  $([[ ${testResults[0]} -eq 0 ]] && echo "passed" || echo "failed")

# tests 2 and 3: Did they both have the *original* conversation?

echo "Test 2: Check if party 1's conversation was the same as original."
diff -u ${party1Dir}/convo/convo.txt ${testDataDir}/fullConvo.txt
testResults[1]=$?
echo "Test 2:"  $([[ ${testResults[1]} -eq 0 ]] && echo "passed" || echo "failed")

echo "Test 3: Check if party 2's conversation was the same as original."
diff -u ${party2Dir}/convo/convo.txt  ${testDataDir}/fullConvo.txt
testResults[2]=$?
echo "Test 3:"  $([[ ${testResults[2]} -eq 0 ]] && echo "passed" || echo "failed")

############################################################
## }}}
############################################################
