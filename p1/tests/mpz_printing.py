# Requires the following:
# - python 3. I'm using 3.5.2, which is available on arch linux.
# - gmpy2 library. Available on arch linux.
# - GDB version 7.11 or greater (that's the version I'm using).
# - To start using the commands in GDB, type in `source
#   mpz_printing.py`, or whatever you've named this file in GDB's
#   prompt.
# - To figure out the command names and a little about how to use
#   them, type in `help gmpcmd` while in GDB.
# - If you don't like the names of the commands, you can change them.
#   There are fields called `name` in each class. Change the value of
#   this field to change the name of the command.
# - **Don't touch gmpcmd_subCommand class** unless you've got a good
#   reason or you've read about what you're doing. It's not terribly
#   important to your use experience, it just makes creating new
#   commands easier. I use this class to allow subcommands to share
#   resources with each other and to make their relationship as
#   subcommands of 'gmpcmd' explicit in the code.

import gmpy2;
from gmpy2 import mpz;
import re;

# Handling re-sourcing the script:
if 'gmpcmd_subCommand' in dir():
    keep_last = gmpcmd_subCommand._gmpcmd_subCommand__lastResult;
    keep_variables = gmpcmd_subCommand._gmpcmd_subCommand__variables.copy();

############################################################
## Utility Functions for Python: {{{
############################################################

# You can look up symbols directly from gdb instead of using the
# stupidness with printing a value first.
# However, once you look up the symbol, you have to evaluate it's
# value, and doing so requires knowledge of the stack frame where it's
# stored. GDB won't assume that for you. Not sure why this is the
# case. 
def gdb_getValue(name):
    #gdb.execute("print {0}".format(name) );
    #return gdb.history(0);
    symbol = gdb.lookup_symbol(name)[0];
    if (symbol == None):
        return None;
    return symbol.value( gdb.selected_frame() );

#mpz_int is a gdb object here.
def mpz_get_str(mpz_int):
    startLoc = mpz_int['_mp_d'];
    numLimbs = mpz_int['_mp_size'];
    limbSize = 8
    if (numLimbs == 0):
        return '0';
    
    # This object allows you to read from memory addresses of gdb's
    # current inferior process.
    # So the function call 'context.read_memory(<address>, 1)' returns
    # an identical result to gdb's 'examine/1bh <address>'
    # Note that the 'b' in the examine call just restricts the output
    # examine to bytes, and read_memory returns bytes, specifically.
    context = gdb.selected_inferior();
    memList = context.read_memory(startLoc, limbSize*numLimbs).tolist();
    # Stored in little endian way, so first byte is least significant.
    memList.reverse();
    return ''.join( list( map( lambda i: str(i.hex()), memList ) ) );

def mpz_print(mpz_int):
    print( mpz_get_str(mpz_int) );

def mpz_get(mpz_int):
    return gmpy2.mpz( int( mpz_get_str(mpz_int), base=16 ) );

def char_buffer_to_hex_string(pointer, length):
    context = gdb.selected_inferior();
    memList = context.read_memory(pointer, length).tolist();
    memList.reverse();
    return ''.join( list( map( lambda i: i.hex(), memList ) ) );

def char_buffer_to_mpz(pointer, length):
    if length == 0:
        return gmpy2.mpz(0);
    return gmpy2.mpz( int( char_buffer_to_hex_string(pointer,length), base=16 ) );

def coerceToMpz(value):
    if isinstance(value, gdb.Value):
        if value.type.name == 'mpz_t':
            value = mpz_get(value);
        else:
            value = gmpy2.mpz( int( value ));
    elif isinstance(value, str):
        value = mpz(value);
    return value;

############################################################
## }}}
############################################################

############################################################
## Definitions of Custom GDB Commands {{{
############################################################

# NOTE: help gmpcmd will print out a list of all subcommands, and help
# text for those commands. HOWEVER, it'll only print the first line,
# reserving the whole string for `help gmpcmd <cmd>`.
class gmpcmd(gdb.Command):
    ("Allows printing and arithmetic on gmp arbitrary precision integers.\n\n"
    "Features:\n"
    "- Run code for pritning outside of your actual program. If\n"
    "  you're tracking down a bug, running code in your program could\n"
    "  change how the bug behaves if it's a memory error, or it could\n"
    "  cause your program to segfault. Working with the integers\n"
    "  outside of your program is safer.\n"
    "- Various arithmetic commands as listed below. They accept\n"
    "  arguments in the same way as the commands in C.\n"
    "- Create gmp integers straight from character buffers, so that\n"
    "  you know the value that's already stored in there, and you\n"
    "  know what to expect when gmp reads the buffer.\n"
    "- Store the values of gmp integers outside of your program, so\n"
    "  that they're available for inspection no matter what your\n"
    "  program's currently doing.\n"
    "Warnings:\n"
    "Careful about passing it values. It can understand the member\n"
    "operators `.` and `->` but not much else. Don't try to pass it\n"
    "arithmetic (like `p - q`) or references as names ( `&p` ) or\n"
    "derefrences as names ( `*p` ). Whatever you pass in should be:\n"
    """ 
        - A name in your actual program ( eg. such as 'p' of `int p`)
        - A member of something in your actual program (eg. `K.p` of
          `RSA_KEY K`, or `K.p._mp_d`, for that matter.
        - A name that this utility directly recognizes such as `$last`
          or `$gmp<varname>`, or numbered convenience variables (eg.
          $20).  The convenience variables have to at least roughly
          match the operand's type. So pass things that are numbers
          where numbers are expected.
    """
    )
    name = "gmpcmd"
    def __init__(self):
        # - The gdb.COMMAND_SUPPORT is a command type for gdb. It
        #   helps gdb classify this new command that I'm making.  The
        # - gdb.COMMAND_NONE is a completion type for gdb. It helps
        #   gdb figure out how to perform tab completion on commands.
        super().__init__(self.name, gdb.COMMAND_SUPPORT,
                gdb.COMMAND_NONE, True);

# - Makes relationship between commands and subcommands simple and
# explicit.
# - To create new subcommand of 'gmpcmd', just create a class which
#   inherits from this.
# - Do not override __init__ function w/o calling it.
# - Set the 'name' field for each subclass to give the subcommand a
#   name.
# - Do not instantiate directly.
class gmpcmd_subCommand(gdb.Command):
    # Tells __init__ which gdb command is the super command.
    # Helps organize the gdb command names for ease of use.
    superCommandName = gmpcmd.name;
    def __init__(self):
        super().__init__(
                "{0} {1}".format(self.superCommandName, self.name),
                gdb.COMMAND_SUPPORT, 
                #gdb.COMPLETE_SYMBOL,
                #False);
                );

    # Quickly print error message about wrong # of ops.
    BadOps = "Function requires {0} arguments, but was given {1}.".format;

    ############################################################
    ## Storage Functionality: {{{
    ############################################################

    # Everything in here is share between ALL gmpcmd subcommands. They
    # can all access these and all mutate them.

    # Last Result Holder: For quick storage
    # - All gmp subcommands modify the SAME instance of
    #   '__lastResult'.  Therefore, you can do something like: `gmp
    #   add 0 1; gmp sub $last 1` and have the output of the last
    #   command be 0.
    __lastResult = 0;
    def setLastResult(self, result):
        #self.__lastResult = result; #Overrides only w/in calling class.
        gmpcmd_subCommand._gmpcmd_subCommand__lastResult = result;
        #_gmpcmd_subCommand.lastResult = result; #Overrides only w/in calling class.
    def getLastResult(self):
        return gmpcmd_subCommand._gmpcmd_subCommand__lastResult;

    # Holds Names of different variables. You can access them by
    # typing in $gmp<variable name>. That'll get resolved to one of
    # the variables in here.
    # Defining a new variable just takes the result of the last
    # operation. This way I don't have to write some sort of
    # assignment syntax.
    __variables = {};
    def DefineNewVariable(self, variableName):
        gmpcmd_subCommand._gmpcmd_subCommand__variables.update(
            [(variableName, gmpcmd_subCommand._gmpcmd_subCommand__lastResult)] );

    def GetGMPVariable(self, variableName):
        try:
            return gmpcmd_subCommand._gmpcmd_subCommand__variables[variableName];
        except KeyError:
            print("Variable '{0}' does not exist.".format(variableName));
            return None;

    # So that I can get the list without typing something ungodly
    # long. Plus all subclasses will be able to access the list
    # in a uniform way, just in case the method of access changes.
    def GetGMPVariables(self):
        return gmpcmd_subCommand._gmpcmd_subCommand__variables;

    def DeleteVariable(self, variableName):
        try:
            del gmpcmd_subCommand._gmpcmd_subCommand__variables[variableName];
        except KeyError:
            print("Variable '{0}' does not exist or was already deleted."
                    .format(variableName)
                    );

    ############################################################
    ## }}}
    ############################################################

    # NOTE: In Python API, no difference beteen `variable.field` and
    # `variable->field`. GDB handles that for you. All the fields are
    # just entries in the value's dictionary.
    def resolveName(self, fullName):
        # If special symbol for last result:
        match = re.fullmatch(r'\$last', fullName)
        if match:
            return self.getLastResult();
        # If history convenience variable:
        match = re.fullmatch(r'\$(\d+)', fullName)
        if match:
            return gdb.history( int( match.group(1) ) );
        # If a digit string, then just use the digit string.
        if re.fullmatch(r'(0x)?\d+',fullName):
            return fullName;
        # If gmp variable name:
        match = re.fullmatch(r'\$gmp(\w+)', fullName);
        if match:
            name = match.group(1);
            return self.GetGMPVariable( name );
        # Split string at every member access operator:
        # - `.` for direct member
        # - `->` for member of pointed to object.
        tokenList = re.split(r'->|\.', fullName);
        fieldsOf = tokenList[1:];
        mainName = tokenList[0];
        sym = gdb.lookup_symbol(mainName)[0];
        if (sym == None):
            return None;
        # Calculation of value needs a stack frame.
        # Does not assume which frame that is.
        value = sym.value( gdb.selected_frame() );
        for name in fieldsOf:
            value = value[name];
        return value;

    def basicProcessOps(self, arguments):
        args = gdb.string_to_argv(arguments);
        if len(args) != self.numOps:
            return None;
        ops = list( 
            map( lambda i: coerceToMpz( self.resolveName( i ) ), args)
        );
        return ops;

    # Quickly checks of one of the operands is 'None'. Stops a gdb
    # command from trying to perform computations on a non-existent
    # operand.
    def CheckOperands(self, operands):
        if any( list( map(lambda i: i == None, operands) ) ):
            return False;
        return True;

    def complete(self, text, word):
        currentWord = gdb.string_to_argv(text)[-1]
        # Sometimes, the last word just isn't actually returned.
        if currentWord.startswith('$gmp'):
            cp = list( 
                map( lambda i: 'gmp' + str(i) if str(i).startswith(currentWord[4:]) else None,
                    self.GetGMPVariables().keys() ) );
            #print(cp);
            return cp;
        return gdb.COMPLETE_EXPRESSION;
        #return ['pass', 'passer'];
        # If not all of the strings match part of the current word,
        # then no completion occurs.

class gmp_printer(gmpcmd_subCommand):
    ("Prints mpz_t integer.\n"
    "Pass a single gmp integer, or integer pointer as an argument. Will "
    "print out integer in base 10.")

    name = "print";
    numOps = 1;
    def invoke(self, arguments, from_tty):
        ops = self.basicProcessOps(arguments)
        if ops == None:
            print(self.BadOps(self.numOps, len( gdb.string_to_argv(arguments) )));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print( ops[0] );

class gmp_add(gmpcmd_subCommand):
    ("Adds two mpz_t integers.\n"
    "Adds first operand to second operand.\n"
    "Arguments:\n"
        "\t- op1 : mpz_t integer\n"
        "\t- op2 : mpz_t integer\n"
    "Result: op1 + op2\n")

    name = "add"
    numOps = 2;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print( ops[0] + ops[1] );
        super().setLastResult(ops[0] + ops[1]);

class gmp_sub(gmpcmd_subCommand):
    """Subtract two mpz_t integers.
Subtracts second operand from first operand.
Arguments:
    - op1 : mpz_t integer
    - op2 : mpz_t integer
Result:
    op1 - op2
"""
    name = "sub";
    numOps = 2;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print(ops[0] - ops[1]);
        super().setLastResult(ops[0] - ops[1]);

class gmp_mul(gmpcmd_subCommand):
    name = "mul"
    numOps = 2;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print(ops[0] * ops[1]);
        super().setLastResult(ops[0] * ops[1]);

class gmp_floor_div(gmpcmd_subCommand):
    name = "fdiv"
    numOps = 2;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print( f_div(ops[0], ops[1]) );
        super().setLastResult( f_div(ops[0], ops[1]) );

class gmp_floor_div_2_exp(gmpcmd_subCommand):
    name = "fdiv_2exp"
    numOps = 2;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print( f_div_2exp(ops[0], ops[1]) );
        super().setLastResult( f_div(ops[0], ops[1]) );

class gmp_powm(gmpcmd_subCommand):
    """Modular exponentiation.
Calculates op1**op2 mod op3.
Arguments:
    - op1 : mpz_t integer
    - op2 : mpz_t integer
    - op3 : mpz_t integer
Result:
    op1^op2 mod op3
"""
    name = 'powm';
    numOps = 3;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print(gmpy2.powmod(ops[0], ops[1], ops[2]) );
        super().setLastResult(gmpy2.powmod(ops[0], ops[1], ops[2]) );

class gmp_mod(gmpcmd_subCommand):
    """Modular arithmetic.
Calculates op1 mod op2.
Arguments:
    - op1 : mpz_t integer
    - op2 : mpz_t integer
Result:
    op1 mod op2
"""
    name = 'mod';
    numOps = 2;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments);
        ops = self.basicProcessOps(arguments);
        if ops == None:
            print(self.BadOps(self.numOps, len(args)));
            return;
        if not self.CheckOperands(ops):
            print("At least one operand was bad. Terminating '{0}' early"
                    .format(self.name));
            return;
        print( gmpy2.t_mod(ops[0], ops[1]) );
        super().setLastResult( gmpy2.t_mod(ops[0], ops[1]) );

class gmp_print_buffer_as_int(gmpcmd_subCommand):
    """Prints contents of character array.
Takes a character array, transforms it into an mpz_t integer like `mpz_import`, then prints the result. Bytes are considered 'little endian', so first byte is smallest byte and last byte is largest byte. This is how `BYTES2Z` currently does it. Prints it in base 10. 
Note: If length is 0, then 0 is printed.
Note: If pointer arithmetic is needed, you can use gdb to do it, then use the name of the convenience variable in lieu of the 'length argument'
Example:
    print (void *)charPtr - 20
    $25 = 0x5555557598d0
    gmpcmd print_buffer_as_int $25 20
    This would print out characters from address 0x5555557598d0 to 0x5555557598e3
    (charPtr - 20) to (charPtr - 20 + 19)
Arguments:
    - char * ptr
    - buffer length 
"""
    name = 'print_buffer_as_int';
    def invoke(self, arguments, from_tty):
        args =  gdb.string_to_argv(arguments);
        op1 =  self.resolveName(args[0]);
        op2 =  int( self.resolveName(args[1]) );
        #print( "Value of: op1", op1 );
        #print( "Value of: op2", op2 );
        print( char_buffer_to_mpz(op1, op2) );

class gmp_store_buffer_as_int(gmpcmd_subCommand):
    """Stores contents of character array as last result of arithmetic operation.
    Creates mpz_t integer out of character array just like print_buffer_as_int command, but rather than simply print it out, it also makes it the result of the last command so that it can be used in an arithmetic operation later.
Note: If length is 0, then 0 is stored and printed.
Arguments:
    - char * ptr
    - buffer length 
"""
    name = 'store_buffer_as_int';
    def invoke(self, arguments, from_tty):
        args =  gdb.string_to_argv(arguments);
        op1 =  self.resolveName(args[0]);
        op2 =  int( self.resolveName(args[1]) );
        print( char_buffer_to_mpz(op1, op2) );
        super().setLastResult( char_buffer_to_mpz(op1, op2) );

class gmp_showLast(gmpcmd_subCommand):
    """Prints the last result of a gmpcmd arithmetic command.
Does not affect the last result, so you can use it just to double-check. Takes no arguments."""
    name = 'showLast';
    def invoke(self, arguments, from_tty):
        print( gmpcmd_subCommand._gmpcmd_subCommand__lastResult);

class gmp_defineNewVar(gmpcmd_subCommand):
    """Defines or redefines a variable using `$last` as value.
Creates or redefines a variable that lives only in the script that created the python commands. You can use them where you'd use numbers or symbols from the program itself. You type in the name that you want to use.
To refer to a variable with the name <name>, type $gmp<name> and it will resolve to that variable.
The variable will take the value of whatever's currently in `$last`.
Arguments:
    - string: variable name.
"""
    name = 'defVar';
    numOps = 1;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments)
        if len(args) != self.numOps:
            print(self.BadOps(self.numOps, len( gdb.string_to_argv(arguments) )));
            return;
        name = args[0];
        self.DefineNewVariable(name);

class gmp_deleteVar(gmpcmd_subCommand):
    """Deletes an gmpcmd variable.
Deletes one of the variables that was in use by this script.
Arguments:
    - string: variable name
"""
    name = 'delVar'
    numOps = 1;
    def invoke(self, arguments, from_tty):
        args = gdb.string_to_argv(arguments)
        if len(args) != self.numOps:
            print(self.BadOps(self.numOps, len( gdb.string_to_argv(arguments) )));
            return;
        name = args[0];
        self.DeleteVariable(name);

class gmp_printVariables(gmpcmd_subCommand):
    """Prints a list of variables contained within this script.
Arguments: None
"""
    name = 'listVar'
    def invoke(self, arguments, from_tty):
        variables = self.GetGMPVariables();
        for key in variables:
            print('{0} = {1}'.format(key, variables[key]) );


############################################################
## }}}
############################################################

# Instantiating objects of these classes register them with GDB so
# that you can use the commands.
# Prefix commands have to be registered before their subcommands.
gmpcmd(); # Prefix command.
gmp_printer();
gmp_add();
gmp_sub();
gmp_mul();
gmp_floor_div();
gmp_floor_div_2_exp();
gmp_powm();
gmp_mod();
gmp_print_buffer_as_int();
gmp_store_buffer_as_int();
gmp_defineNewVar();
gmp_deleteVar();
gmp_printVariables();
gmp_showLast();

# If script re-sourced:
if 'keep_last' in dir():
    gmpcmd_subCommand._gmpcmd_subCommand__lastResult = keep_last;
if 'keep_variables' in dir():
     gmpcmd_subCommand._gmpcmd_subCommand__variables = keep_variables;

# TODO:
# - Look into how program translates references to history variables.
#   Try to make sure the behavior is consistent across the board, then
#   document that behavior.
