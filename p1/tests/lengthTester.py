############################################################
## Preliminary Stuff {{{
############################################################

def gdb_break(breakPoint):
    gdb.execute("break {0}".format(breakPoint) );

#def gdb_getValue(name):
    #gdb.execute("print {0}".format(name) );
    #return gdb.history(0);

def gdb_setValue(name, value):
    gdb.execute("set variable {0} = {1}".format(name, value) );
    actual = gdb_getValue(name)
    if actual != value:
        print("Unable to set variable '{0}' to value '{1}'"
                .format(name, value) );
        raise Exception();

def action_46():
    gdb_setValue(keyLengthVarName, 320);
def debugPrints():
    gdb_setValue('debugPrints', 0);
def encrypt():
    gdb.execute('up');
    gdb.execute('gmpcmd store_buffer_as_int pt mLen');
    gdb.execute('gmpcmd defVar fullpt');
    gdb.execute('down');
def main_returnLine():
    pass;


breakpoints = [
    {
        'description' : 'rsa_keyGenLine',
        'point'       : 46,
        'action' : action_46,
    },
    {
        'point' : 'rsa.c:91',
        'description' : 'rsa_debugPrintsLine',
        'action' : debugPrints,
    },
    {
        'point'          : 'rsa.c:rsa_encrypt',
        'description' : 'rsa_encrypt',
        'action' : encrypt,
    },
    #{
        #'point'      : 89
        #'description' : 'main_returnLine'
        #'action' : pass;
    #},
    #{'rsa_decrypt'          : 'rsa.c:rsa_decrypt'},
];

keyLengthVarName = 'keyLength';

############################################################
## }}}
############################################################

gdb.execute('start');
gdb.execute('source mpz_printing.py');
for point in breakpoints:
    gdb_break(point['point']);
    gdb.execute('continue');
    point['action']();



