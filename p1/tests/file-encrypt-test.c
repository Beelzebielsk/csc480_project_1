#include <stdio.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h> // malloc
#include <fcntl.h> // open, I think.

#include "../ske.h"
#include "../prf.h"

#define IV_LEN 16
int main() {

	// Set seed for randomization.
	unsigned char seed[32];
	for (int i = 0; i < 32; i++) seed[i] = (i*3+53) & 0xff;
	setSeed(seed,32);
	SKE_KEY K;
	ske_keyGen(&K,0,0);

	size_t pgSize = (size_t) sysconf (_SC_PAGESIZE);
	const char * inFile = "encrypt_test_file.txt";
	const char * outFile = "encrypt_test_file_output.txt";
	const char * dtFile = "decrypted_test_file_output.txt";

	unsigned char IV[IV_LEN];
	for (size_t i = 0; i < IV_LEN; i++) IV[i] = (unsigned char) i;

	puts("Starting file encryption:");
	int cipherLength = 0;
	cipherLength = ske_encrypt_file(outFile, inFile, &K, IV, 0);
	if (cipherLength < 0) {
		fprintf(stderr, "Test failed. Exiting early.\n");
		return cipherLength;
	}

  int ctFile = open(outFile, O_RDONLY);
	unsigned char * dt = malloc(pgSize);
	memset(dt, 0, pgSize);
	unsigned char * ct = (unsigned char *)
		mmap(0, pgSize, PROT_READ, MAP_PRIVATE, ctFile, 0);

	if(ct == (unsigned char *)-1) {
		fprintf(stderr, "Error while testing ciphertext file: %s\n", strerror(errno));
		return -1;
	}

	puts("Decrypting using buffer only:");
	size_t dtLen = ske_decrypt(dt, ct, cipherLength, &K);
	printf("Decrypted Text: %s\n", dt);

	puts("Decrypting using file specifically:");
	ske_decrypt_file(dtFile, outFile, &K, 0);

	free(dt);
	close(ctFile);
	return 0;
}
