#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h> // Includes sysconf.
#include <errno.h>

int main() {
	size_t page_size = (size_t) sysconf (_SC_PAGESIZE);
	printf("Page Size: %lu\n", page_size);

	// Attempt to change the location of the map pointer.
	char * adjustMmapPtrBehind = malloc(10); // No change in location.

	const char * filename = "test.txt";
	int testFileDescriptor = open(filename, O_RDWR);
	
	// The permissions asked for here have to be granted by the file
	// permissions from the 'open' call. If they're not granted, then the
	// mmap function call will fail, and the reported error will be a
	// permission error.
	char * contents =
		(char*)mmap(
				0,
				1,
				PROT_READ | PROT_WRITE,
				MAP_SHARED,
				testFileDescriptor,
				0
			);

	// More attempts to change the ptr value from mmap call.
	int changeMmapPtr = -1;
	char * adjustMmapPtrAhead = malloc(100);
	// Both attempts unsuccessful.
	
	printf("Pointer value: %lx\n", (long unsigned)contents);
	if (contents != -1){ 
		contents[0] = 'i';
		printf("File Length: %lu\n", strlen(contents));
		printf("File contents: %s", contents);
	} else {
		printf("Error: %s\n", strerror(errno));
	}
	printf("Blank Length: %lu\n", strlen(""));
	return 0;
}

// Now mmap makes sense. Because mmap makes use of virtual memory, it'll
// map virtual memory addresses to bytes of a file (that's why it's
// called a map).
// However, that means that it has to map at least a whole page of
// memory. It can't do any less than that.
//
// Looking up a page size on my laptop (GOONIESCHOOL), I see that the
// page size is 4096 bytes. Once I made a file that's longer than 4096
// bytes, I saw that the whole file was no longer mapped.
// In particular, I made sure that the file contained exactly 512 lines,
// each 8 bytes long, meaning that the entire first part was 4096 Bytes
// long. Then I put a test line right afterward: 'Can you see me?'.
// When I used mmap on the file, starting from the file's beginning (ie
// no offset), I couldn't see the last line because it was right outside
// of the page that was mapped.
