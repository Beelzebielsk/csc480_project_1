#include <stdio.h>
//#include <sys/mman.h>
#include <openssl/evp.h>
#include <string.h>

#define KLEN_SKE 32
void printBinaryString(unsigned char * binString, size_t len);

int main() {
	// Starting stuff:
	unsigned char key[KLEN_SKE];
	memset(key, 0, KLEN_SKE);

	unsigned char iv[16];
	for (int i = 0; i < 16; i++) iv[i] = i;

	char * testString = "0123456789";
	EVP_CIPHER_CTX * encctx = EVP_CIPHER_CTX_new();
	EVP_CIPHER_CTX * decctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(encctx, EVP_aes_256_ctr(), 0, key, iv);
	EVP_DecryptInit_ex(decctx, EVP_aes_256_ctr(), 0, key, iv);

	size_t len = strlen(testString) + 1; // for null pointer.
	unsigned char * ct = malloc(len*3);
	unsigned char * pt = malloc(len*3);

	int outputLength = 0;
	EVP_EncryptUpdate(encctx, ct, &outputLength, (unsigned char*)testString, len);
	printf("Unsigned test string: %s\n", (unsigned char *)testString);
	printf("Output Length of ct: %d\n", outputLength);
	printf("Hex of ct: ");
	printBinaryString(ct, outputLength);
	puts("");
	EVP_DecryptUpdate(decctx, pt, &outputLength, ct, outputLength);
	printf("Output Length of pt: %d\n", outputLength);
	printf("Hex of pt: ");
	printBinaryString(pt, outputLength);
	puts("");
	printf("Contents of pt: %s\n", pt);

	EVP_CIPHER_CTX_free(encctx);
	EVP_CIPHER_CTX_free(decctx);

	// Now to try it piece-by-piece.
	
	// Start them back up.
	encctx = EVP_CIPHER_CTX_new();
	decctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(encctx, EVP_aes_256_ctr(), 0, key, iv);
	EVP_DecryptInit_ex(decctx, EVP_aes_256_ctr(), 0, key, iv);

	unsigned char * piecect = malloc(len*3);
	unsigned char * piecept = malloc(len*3);
	int totalOutputLen = 0;

	//Bit-by-bit encryption.
	for(int i = 0; i < len; i++) {
		EVP_EncryptUpdate(encctx, piecect+i, &outputLength, testString + i, 1);
		totalOutputLen += outputLength;
	}
	printf("Hex of piecect: " );
	printBinaryString(piecect, totalOutputLen);
	puts("");

	totalOutputLen = 0;
	//Bit-by-bit decryption.
	for(int i = 0; i < len; i++) {
		EVP_DecryptUpdate(decctx, piecept+i, &outputLength, piecect+i, 1);
		totalOutputLen += outputLength;
	}
	printf("Hex of piecept: " );
	printBinaryString(piecect, totalOutputLen);
	puts("");

	printf("Contents of piecept: %s\n", piecept);

	return 0;
}

void printBinaryString(unsigned char * binString, size_t len){
	for (size_t i = 0; i < len; i++){
		fprintf(stderr, "%02x",binString[i]);
	} fprintf(stderr, "\n");
}
