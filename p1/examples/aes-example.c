/* dumb example to illustrate AES in cbc and ctr modes. */
#include <stdio.h>
#include <string.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/err.h>

int cbc_example()
{
	unsigned char key[32];

	// Generate some throwaway key.
	size_t i;
	for (i = 0; i < 32; i++)
		key[i] = i;

	// Declare keys, pass their references into the key setting
	// functions called later.
	AES_KEY kenc;
	AES_KEY kdec;
	AES_set_encrypt_key(key,256,&kenc);
	AES_set_decrypt_key(key,256,&kdec);
	char* message = "this is a test message";
	size_t len = strlen(message);

	// Is this not the ceiling function?
	// I suppose it's basically the ceiling function
	// for use with integers only.
	// ctLen seems to be th length of the cipherText in
	// AES blocks, however large they are.
	// AES_BLOCK_SIZE is set to 16, so I suppose that means
	// 16 bytes, since 'len' returns length in bytes.
	// AES_BLOCK_SIZE is a macro that's defined in
	// <openssl/aes.h>.
	size_t ctLen = (len/AES_BLOCK_SIZE +
			(len%AES_BLOCK_SIZE?1:0)) * AES_BLOCK_SIZE;
	
	// These are output and input buffers. The algorithm is first going
	// to read our message from the *actual message*, then write into 
	// the 'ct' buffer. These lenghts are probably just bullshit lengths
	// to make absolute sure that he can catch the output of the
	// function either time. I can't see any reason for this right now
	// beyond just easy ways to catch output no matter what he sets the
	// output to be.
	// TODO: Test this assumption.
	unsigned char ct[512];
	unsigned char pt[256];
	/* so you can see which bytes were written: */
	memset(ct,0,512);
	memset(pt,0,256);
	unsigned char iv[16];
	for (i = 0; i < 16; i++) iv[i] = i;
	/* NOTE: openssl's AES_cbc_encrypt *will destroy the iv*.
	 * So you have to make sure you have a copy: */
	unsigned char iv_dec[16]; memcpy(iv_dec,iv,16);

	// Arguments:
	// unsigned char* in: Input to the algorithm. For Encryption, this
	//	is the plaintext. For Decryption, this is the ciphertext.
	// unsigned char* out: Output from the algorihm. For Encryption,
	//	this is the ciphertext. For Decryption, this is the plaintext.
	// size_t length: Length of the input.
	// AES_KEY *key: Key for encryption or decryption, depending on the
	//	mode of the function.
	// unsigned char* iv: I suppose that's a pointer to some initial
	//	value. Perhaps it's always 16 bytes in length.
	// [0,1] : AES_ENCRYPT is 1 and AES_DECRYPT is 0. Probably a mode
	//	 for the function. I suppose that while it's a symmetric
	//	 encryption, that doesn't mean that the operation to encrypt and
	//	 decrypt is EXACTLY the same. It just means that the key is the
	//	 same.
	AES_cbc_encrypt((unsigned char*)message,
			ct,len,&kenc,iv,AES_ENCRYPT);
	for (i = 0; i < ctLen; i++) {
		fprintf(stderr, "%02x",ct[i]);
	}
	fprintf(stderr, "\n");
	/* note the use of the copied iv_dec, since the original
	 * was modified by the first cbc_encrypt call. */
	AES_cbc_encrypt(ct,pt,ctLen,&kdec,iv_dec,AES_DECRYPT);
	fprintf(stderr, "%s\n",pt);
	return 0;
}

// Counter mode, it seems.
int ctr_example()
{
	unsigned char key[32];
	size_t i;
	for (i = 0; i < 32; i++) key[i] = i;
	unsigned char iv[16];
	for (i = 0; i < 16; i++) iv[i] = i;

	// Buffers to catch the return values of the encryption functions. I'm
	// going to play with these sizes over time to see what the sizes of the
	// input and output actually are.
	unsigned char ct[512];
	unsigned char pt[512];
	/* so you can see which bytes were written: */
	memset(ct,0,512);
	memset(pt,0,512);
	char* message = "aaaaaaaaa";
	size_t len = strlen(message);
	/* encrypt: */

	// Sets up an 'encryption context'
	// It's everything that a cipher needs to encrypt
	// and decrypt information packed into one neat structure.
	// If this fails, then it'll return 'NULL', so watch out for
	// that.
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();

	// Init function returns 1 for success and 0 for failure.
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,key,iv))
		ERR_print_errors_fp(stderr);

	// This variable will hold the number of bytes written to
	// the output.
	int nWritten;

	if (1!=EVP_EncryptUpdate(ctx,ct,&nWritten,(unsigned char*)message,len))
		ERR_print_errors_fp(stderr);
	printf("plaintext Length: %lu\nctLen: %d\n", len, nWritten);

	// We're finished with this context now (though, according to the 
	// professor's below comment, we don't have to be when it comes to
	// this specific cipher, or any cipher where the encryption process
	// and decryption processes are exactly the same.
	EVP_CIPHER_CTX_free(ctx);

  // Display the encrypted text, byte by byte as hexadecimal.
	size_t ctLen = nWritten;
	for (i = 0; i < ctLen; i++) {
		fprintf(stderr, "%02x",ct[i]);
	}
	fprintf(stderr, "\n");
	/* now decrypt.  NOTE: in counter mode, encryption and decryption are
	 * actually identical, so doing the above again would work. */

	// Is that really necessary? Won't this just get updated 
	// by the update function calls?
	// TODO: Ask the professor about this, but until then do exactly
	// that. Clear that variable from invocation to invocation.
	nWritten = 0;
	ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,key,iv))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_DecryptUpdate(ctx,pt,&nWritten,ct,ctLen))
		ERR_print_errors_fp(stderr);
	fprintf(stderr, "%s\n",pt);
	return 0;
}

int main()
{
	return ctr_example();
	// return cbc_example();
}
