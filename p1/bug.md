# Potential RSA Implementation Solutions

- One byte at a time. That might help.
- Send chunkLengths in the clear. Might still be kinda random, but I
	totally doubt it. A determined adversary could probably notice
	patterns there.
- Since the RSA isn't for public use, ensure that the RSA key length
	is much larger than the keys for the asymmetric encryption. I can
	enforce that in my own program and sidestep all of this bullshit.
	Besides, I've yet to find a single concrete implementation that
	handles streams of bytes instead of single numbers, which is easy as
	hell to do.
- Instead of trying to smush all the ciphertexts into one huge
	confusing ciphertext, actually create an array of ciphertexts.
	Basically, the 'encrypt' and 'decrypt' functions will be solely for
	the use of messages which can be encrypted in one shot. I can make a
	different function for plaintexts for which this isn't the case, in
	which case I can do the following: Full Message: Has # of Bytes as n
	- 1 2 Bytes | Rest of Bytes Total Size | plaintext Decrypt that,
		then read the first two bytes to figure out the size, then use
		that size to reconstruct the full plaintext with leading zeroes
		and all.  Example: Suppose that # of bytes in n is 40. Then full
		message length is 39 bytes, meaning that I can process 37 bytes of
		plaintext.  TODO: Wait, if I always process the same amount of
		plaintext, then why do I need a size? The size is necessarily
		constant. I can just derive the size from the keylength. Chances
		are, just sending a series of fully encrypted messages instead of
		a concatenation of a bunch of messages is enough of a solution
		that I won't need to include plaintext lengths.

Acccepted Solution: 

Create each RSA ciphertext like so:

+-------------------------+----------------+
| 2 Bytes                 | Rest of Bytes  |
+-------------------------+----------------+
| plain ciphertext length | RSA(len(x)||x) |
+-------------------------+----------------+

And the process for decryption is:

1. Read in first 2 bytes as ciphertext length.
2. Use that length as the `len` argument to the RSA function.
3. Have the RSA decryption function perform the normal decryption
	 method on the specified bytes, then read the first two bytes of the
	 decrypted result. Interpret that as the original length of the
	 plaintext. Compare the resulting plaintext's length with the length
	 obtained from those first two bytes. Fill in missing bytes with
	 `0x00` since those are necessarily the only bytes that would be
	 lost: leading zero bytes wouldn't affect the number read in by GMP,
	 and therefore wouldn't affect the result of the modular arithmetic.


# Potential RSA Bug Theories

- So far what I've seen is that my implementation cuts off leading zero
  bytes on the plaintext. It could be that printing changes the region
  in memory right after the decrypted text. Without printing, it's 0,
  and with printing, it's nonzero, thus causing the different results.

# RSA problems

- The `rsa_numbytesN` function leaves something to be desired. It
	returns the total # of bytes used to store `n`, but that may be more
	bytes than it actually has.

# Notes

- The smallest key length that's divisible by 16 for which the rsa
	test fails is: **16 Bytes** which is **128 bits** ($8\cdot8\cdot2$
	bits).
	- Small note: the key test was failing because one of the generated
		primes with the supplied random data is '7', which is one of the
		test primes.
- When key length is 2 Bytes (16 bits), gmp printing of 1 byte numbers
	has a segmentation fault. Figure out why that might be the case.
- List of bit lengths that fail with printing, but succeed without:
	- 320
	- 384
	- 448
	- 512
	- 576
	- 640
	- 704
	- 768
	- 896
	- 960
	- 1024
- Other weird lengths:
	- 192: Fails 6 tests under both conditions.
	- 832: Always fails when printing, but fails just once when not
		printing.


# Creation of Test

- Automating the test:
	- Try defining 'print' and 'noprint' versions of the rsa functions.
		See if having both of them defined causes any difference in
		behavior.
		- Works! I made some conditional logic for printing.
	- Start GDB.
	- Create a list of keyLengths.
	- Loop:
		- Start program.
		- Break at line 46 'rsa_keygen'.
		- Run the printing keygen.
		- Make 'keylength' equal to current keyLength of list in GDB.
		- Continue test until failure or success is determined.
			- Since I apparently can't depend on the return value of 'main',
			then break right before the return value and read the value of
			'rsaTestFail'.
				- If test failed, then mark down the bitLength somewhere.
		- restart program again, breaking at line 46 again, and setting
			the same keyLength again. However, run the nonprinting keygen,
			now.
		- Continue test until failure or success is determined.
		- If test failed first time, but succeeded second time, list down
			the number.
- Goals for automated test:
	- Test for consistency of results under each condition. Try to see
		if they occur the same way on every run of the program, so run the
		program multiple times and record the results of every run.

Interested in:

- Key Length
- Whether or not printing occured.
- Did it fail...
	- Never?
	- Consistently?
	- Neither
	
- Fails all the time under all conditions
- Fails all the time under a certain condition. Make a list per
	condition.
- Fails inconsistently under all conditions.
- Fails inconsistently under a certain condition. Make a list per
	condition.
- Fails never under all conditions.
- Fails never under a certain condition. Make a list per condition.

# Python API Notes

- When using the read_memory function, you can add numbers to the
  memory location that you pass to the function. When doing so, it
  doesn't add '1' to the location, it performs pointer arithmetic,
  just like in C/C++. So when I add 1 to the pointer of an
  `mpz_limb_t`, I'm adding 8 to the memory address.
- When I execute this script, stuff that gets defined, stays defined
  if I want to call it again using gdb's 'python' command. So I can
  define functions here, then call them from within gdb by typing `gdb
  <funcName>(<args>)`.
- In fact, when you execute ANYTHING using 'source' or python, that
  change stays. This is... good.

