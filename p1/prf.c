#include "prf.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <gmp.h>

/* PRF state: */
#define BLOCK_LEN 64 /* we will use SHA512 */
// ANS: 1 byte. 64 bytes is 2^6 * 2^3 bits which is 512 bits. What
// this defines is the length, in bytes, of the output of an sha512
// hash function so that we know how many times to hash numbers in
// order to get the number of bytes that we need. If we want 1024
// bytes of random numbers, then we'd need to call sha512 2^10 / 2^6 =
// 2^4 = 16 times. These 'blocks' are the blocks of random bytes that
// we can spit out at any one time.

// This keeps track of the number of times that we've requested a
// random number. Interestingly enough, we're not directly calling on
// /dev/urandom for random numbers. We're just stepping through the
// different possible values that we can pass to sha512, starting from
// 1. We'll arguably never get the same value twice this way.
// Interesting.
static mpz_t rcount;

static unsigned char rkey[BLOCK_LEN];
static int prf_initialized;
/* NOTE: for those that don't know, static variables in C will be
 * initialized to 0.  This is important for us. */
#define VERY_YES 9191919

int setSeed(unsigned char* entropy, size_t len)
{
	if (prf_initialized != VERY_YES) mpz_init(rcount);

	// Set rcount to 1, interpreting 1 as an unsigned integer.
	mpz_set_ui(rcount,1);
	int callFree = 0;

	// If the pointer 'entropy' is null, meaning that you passed this
	// thing jack all to work with, then it creates some entropy for
	// you:
	if (!entropy) {
		// After setting this to 1, we'll have to call the 'free' method
		// at the end, since we created the entropy within this function.
		// Also, there's really no reason to keep the entropy around.
		callFree = 1;

		// Create an array 32 bytes long.
		// Note that this is an argument to the function. This is set in
		// case you haven't provided it with any actual entropy.
		len = 32;
		entropy = malloc(len);

		// Open the urandom file for:
		//	'r': reading
		//	'b': Not sure what the 'b' is for. I would think binary, but
		//	according to man 3 fopen, the 'b' is ignored on all POSIX
		//	compliant systems.
		FILE* frand = fopen("/dev/urandom", "rb");

		// Read 'len' chunks each '1' byte long from the 'frand' file into
		// the 'entropy' character buffer.
		fread(entropy,1,len,frand);
		fclose(frand);
	}

	// Hm. Why use the hash here? As much as the hash is intended to
	// behave like a random function, so is reading from /dev/urandom.
	// Perhaps the reason is that the professor read from /dev/urandom,
	// which could potentially return a shitty random number since it
	// won't wait for more entropy. Another reason might be to normalize
	// the length of the seed. sha512 will always create 512 bits.
	// ASK: Why use sha512 here?
	// I think that the 'rkey' is essentially our seed.
	// Yeah, it is, because it's the secret key for the HMAC function.
	// And our HMAC'ed hashing function is essentially our pseudorandom
	// number generator.
	SHA512(entropy, len, rkey);
	if (callFree) free(entropy);
	prf_initialized = VERY_YES;
	return 0;
}

// 'len' is in bytes.
int randBytes(unsigned char* outBuf, size_t len)
{
	if (prf_initialized != VERY_YES) setSeed(0,0);
	size_t nBlocks = len / BLOCK_LEN;
	size_t i;
	for (i = 0; i < nBlocks; i++) {

		// Source: https://www.openssl.org/docs/manmaster/crypto/HMAC.html
		// HMAC arguments:
		// - const EVP_MD *evp_md
		//	This is a hashing function to pass to HMAC. I suppose the
		//	point is that the actual procedure for HMAC is the same,
		//	independent of the hashing function. So you can pass it all of
		//	the inputs and the actual hashing function to use, it'll
		//	generate the random coefficients, perform the hashing and
		//	return the output.
		//	Chosen function is sha512.
		// - const void *key
		//	The key for the HMAC.
		//	Our key is 'rkey', which is 64 bytes long.
		// - int key_len
		//	The length of the HMAC key. Our key length is 64 bytes, since
		//	it's an array of 64 unsigned characters.
		// - const unsigned char* d
		//	Input to the hashing function itself. The first 'n' bytes from
		//	this array are sent to the hasing function. What's passed here
		//	is a pointer to the array of limbs of our arbitrary precision
		//	integer, and we cast it as an unsigned char pointer so that
		//	the hashing function can read it byte-by-byte. Our input is
		//	'rcount', ultimately, but massaged a bit so that HMAC can use
		//	it.
		// - int n
		//	Uses every available byte of the arbitrary precision integer.
		//	calculates number of bytes by multiplying (# of limbs) by
		//	(size of each limb in bytes).
		// - unsigned char *md
		//	The output of the hashing function itself.
		//	`outBuf` is the output buffer here.
		// - unsigned int *md_len
		//	The length of the output. It's not constant, so it's going to
		//	change. The function sets the value, letting you know the
		//	length of the output of the hash.
		//	Here, we pass in NULL, because if we don't, then it'll store
		//	the length of the output buffer in some static variable
		//	somewhere. We have no need of that.
		HMAC(EVP_sha512(),rkey,BLOCK_LEN,(unsigned char*)mpz_limbs_read(rcount),
				sizeof(mp_limb_t)*mpz_size(rcount),outBuf,NULL);
		mpz_add_ui(rcount,rcount,1);

		// Now that we've generated a block of 64 random bytes, it's time
		// to move onto the next block. Shift the pointer forward by 64
		// bytes.
		outBuf += BLOCK_LEN;
	}

	/* handle final block: */
	unsigned char fblock[BLOCK_LEN];
	if (len % BLOCK_LEN) {
		HMAC(EVP_sha512(),rkey,BLOCK_LEN,(unsigned char*)mpz_limbs_read(rcount),
				sizeof(mp_limb_t)*mpz_size(rcount),fblock,NULL);
		mpz_add_ui(rcount,rcount,1);

		// So, to handle the final block, we copy the bytes we want into
		// the output Buffer, and we ditch the rest (we don't copy them
		// and fblock is going to get destroyed.
		memcpy(outBuf,fblock,len%BLOCK_LEN);
	}
	return 0;
}

