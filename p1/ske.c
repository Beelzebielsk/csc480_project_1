#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h> // ftruncate
#include <sys/mman.h>
//#include <sys/types.h> // ftruncate
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 *+------------+--------------------+--------------------------------+
 *| 16 byte IV | C = AES(plaintext) |HMAC(IV|C) (32 bytes for SHA256)|
 *+------------+--------------------+--------------------------------+
 * */

// The total length of this message is:
//	 16 bytes for the initial value 
//	 + length of the AES ciphertext
//	 + length of the HMAC of SHA256 hash, which is 32 bytes.
// Making that 48 + ciphertext length.

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{

	// I will assume that the entropy given is for our 'x'. It is the data
	// that will be forged into a key.  The KDF has to be public, I suppose,
	// since we won't be sending the key 'KDF(x)', but 'x' itself.
	
	// In order for two parties to communicate, they need to have:
	// - Same aesKey, so that the message they're sending encrypt/decrypt
	//   the way that they're supposed to.
	// - Same hmacKey, so that they can both verify integrity/authenticity
	//   of ciphertexts. If they don't have the same hmacKey, then they
	//   can't hash the ciphertexts by themselves and reproduce the
	//   message hash. So the KDF really generates *both* keys for the
	//   symmetric encryption algorithm.
	// It follows that the parties need to transmit a 64-byte x, and
	// therefore that this should expect an entropy length of 64 bytes.

	// Starting entropy:
	int freeEntropy = 0;
	int freeNewEntropy = 0;
	unsigned char * newEntropy;
	size_t totalEntropyLength = sizeof(unsigned char)*KLEN_SKE*2;
	unsigned char * hashOutput = malloc(totalEntropyLength);
	unsigned int * hashOutputLength;
	if(!entropy) {
		// For both keys.
		entropy = malloc(totalEntropyLength);
		freeEntropy = 1;
		randBytes(entropy, KLEN_SKE*2);
	} else if ( entLen < KLEN_SKE*2 ) {
		newEntropy = malloc(totalEntropyLength);
		freeNewEntropy = 1;

		// Copy over the old entropy into the new entropy
		memcpy(newEntropy, entropy, entLen);
		randBytes(newEntropy + entLen, totalEntropyLength - entLen);
		entropy = newEntropy;
	}

	HMAC(
			EVP_sha512(),
			KDF_KEY,
			KLEN_SKE,
			entropy,
			totalEntropyLength,
			hashOutput,
			&hashOutputLength
		);
	// Set key values.
	memcpy(K->aesKey, entropy, KLEN_SKE);
	memcpy(K->hmacKey, entropy + KLEN_SKE, KLEN_SKE);

	if( freeEntropy == 1 ) free(entropy);
	if( freeNewEntropy == 1) free(newEntropy);
	
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
	// `AES_BLOCK_SIZE` Defined in openssl/aes.h, and it's 16.
}

// Will generate an IV for you if you pass IV = NULL.
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	
	int IVfree = 0;
	unsigned char * curOut = outBuf;
	if (!IV) {
		IV = malloc(IV_LEN);
		randBytes(IV, IV_LEN);
		IVfree = 1;
	}
	// Fill out first part of final ciphertext.
	memcpy(curOut, IV, IV_LEN);
	curOut += IV_LEN;

	int goodResult;
	int outputLength;
	//int inputLength;
	// Initialize Encryption Context:
	EVP_CIPHER_CTX * aesContext = EVP_CIPHER_CTX_new();
	goodResult = 
		EVP_EncryptInit_ex(aesContext, EVP_aes_256_ctr(), 0, K->aesKey, IV);
	if (!goodResult) ERR_print_errors_fp(stderr);

	goodResult =
		EVP_EncryptUpdate(aesContext, curOut, &outputLength, inBuf, len);
		//EVP_EncryptUpdate(aesContext, curOut, &outputLength, inBuf, 2);
	if (!goodResult) ERR_print_errors_fp(stderr);
	curOut += outputLength;
	EVP_CIPHER_CTX_free(aesContext);

	// Now our output is: IV, C. We just need HMAC(IV|C).
	HMAC(
			EVP_sha256(), //Hash
			K->hmacKey, // Key for hash,
			KLEN_SKE, // Key length
			outBuf, // Input
			IV_LEN + outputLength, // Bytes to read.
			curOut, // output Buffer
			(unsigned int*)&outputLength
		);
		curOut += outputLength;
	
	if(IVfree) free(IV);

	return curOut - outBuf; // Return # bytes written.
}

size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{

	// Values needed throughout function.
	// - retVal: Return value of the function. Typically the return value
	//    of the last function which failed
	size_t retVal = 0;

	struct stat statStruct;
	stat(fnin, &statStruct);
	size_t inputFileSize = statStruct.st_size;

	int inFile = open(fnin, O_RDONLY, S_IRWXU);
	if (inFile < 0) {
		fprintf(stderr, "Error opening plaintext file: %s\n", strerror(errno));
		retVal = -1;
		goto inFailed;
	}
	int outFile = open(fnout, O_RDWR | O_CREAT , S_IRWXU);
	if (outFile < 0) {
		fprintf(stderr, "Error opening ciphertext output file: %s\n", strerror(errno));
		retVal = -1;
		goto outFailed;
	}
	// ftruncate sets file length to the length argument specified.
	// Necessary because mmap will not allow writing beyond the end of a
	// file.
  if ( ftruncate(outFile, ske_getOutputLen(inputFileSize) + offset_out  ) ) {
		fprintf(stderr, "Error during length set of ciphertext file: %s\n", strerror(errno));
		retVal = -1;
		goto end;
	}

	unsigned char * plain = (unsigned char*)
		mmap(0, inputFileSize, PROT_READ, MAP_PRIVATE, inFile, 0);
	if (plain == (unsigned char*)-1) {
		fprintf(stderr, "Error while opening plaintext file: %s\n", strerror(errno));
		retVal = -1;
		goto end;
	} 

	// This is failing for unknown reasons. When passed a non-zero offset,
	// the error code "EINVAL" is returned. This may be because
	// 'offset_out' is not a multiple of the computer's page size.
	// If that's the case, I'm going to instead use the offset directly in
	// the function call for encryption.
	//unsigned char * cipher = (unsigned char*)
		//mmap(
				//0,
				//ske_getOutputLen(inputFileSize),
				//PROT_READ | PROT_WRITE,
				//MAP_SHARED,
				//outFile,
				//offset_out
			//);

	unsigned char * cipher = (unsigned char*)
		mmap(
				0,
				ske_getOutputLen(inputFileSize) + offset_out,
				PROT_READ | PROT_WRITE,
				MAP_SHARED,
				outFile,
				0
			);
			
	if (cipher == (unsigned char*)-1) {
		fprintf(stderr, "Error while opening ciphertext file: %s\n", strerror(errno));
		retVal = -1;
		goto end;
	}

	// Using offset directly at function call, since offset may have to be
	// a multiple of system's page size.
	//retVal = ske_encrypt(cipher, plain, inputFileSize, K, IV);
	retVal = ske_encrypt(cipher + offset_out, plain, inputFileSize, K, IV);
	munmap(plain, inputFileSize);
	munmap(cipher, ske_getOutputLen(inputFileSize));
end:
	close(outFile);
outFailed:
	close(inFile);
inFailed:
	return retVal;
}

size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{

	// Check validity of MAC:
	size_t hashLength = 32;
	unsigned int outputLength;
	int beginningLength = len - hashLength;
	unsigned char * check = malloc(hashLength);
	HMAC(
			EVP_sha256(),
			K->hmacKey,
			KLEN_SKE,
			inBuf,
			beginningLength,
			check,
			&outputLength
		);
	
	// Something went very wrong. The two hashes should be equivalent, so
	// they should also have equivalent lengths.
	// Return code for bad hash is -1.
	if(outputLength != hashLength) return -1;
	for (size_t i = 0; i < hashLength; i++){
		if( check[i] != inBuf[i + beginningLength] ) return -1;
	}

	// At this point, the two hashes are the same, thus we can proceed to
	// decrypt the ciphertext.

	int goodResult;
	EVP_CIPHER_CTX * aesContext = EVP_CIPHER_CTX_new();
	goodResult = 
		EVP_DecryptInit_ex(aesContext, EVP_aes_256_ctr(), 0, K->aesKey, inBuf);
	if (!goodResult) ERR_print_errors_fp(stderr);

	goodResult =
		EVP_DecryptUpdate(
				aesContext,
				outBuf,
				&outputLength,
				inBuf + IV_LEN,
				len - IV_LEN - hashLength
			);
	EVP_CIPHER_CTX_free(aesContext);

	if (!goodResult) ERR_print_errors_fp(stderr);
	return outputLength;
}

size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{

	size_t retVal = 0;
	struct stat statStruct;
	stat(fnin, &statStruct);
	size_t inputFileSize = statStruct.st_size;
	size_t outputFileSize = inputFileSize - offset_in - HM_LEN - IV_LEN;

	int inFile = open(fnin, O_RDONLY, S_IRWXU);
	if (inFile < 0) {
		fprintf(stderr, "Error opening ciphertext file: %s\n", strerror(errno));
		retVal = -1;
		goto inFailed;
	}
	int outFile = open(fnout, O_RDWR | O_CREAT , S_IRWXU);
	if (outFile < 0) {
		fprintf(stderr, "Error opening decrypted output file: %s\n", strerror(errno));
		retVal = -1;
		goto outFailed;
	}

  if ( ftruncate(outFile, outputFileSize) ) {
		fprintf(stderr, "Error during length set of ciphertext file: %s\n", strerror(errno));
		retVal = -1;
		goto end;
	}

	unsigned char * cipher = (unsigned char*)
		mmap(0, inputFileSize, PROT_READ, MAP_PRIVATE, inFile, 0);
	if (cipher == (unsigned char*)-1) {
		fprintf(stderr, "Error while opening ciphertext file: %s\n", strerror(errno));
		retVal = -1;
		goto end;
	} 

	unsigned char * decrypted = (unsigned char*)
		mmap(
				0,
				outputFileSize,
				PROT_READ | PROT_WRITE,
				MAP_SHARED,
				outFile,
				0
			);
	if (decrypted == (unsigned char*)-1) {
		fprintf(stderr, "Error while opening decrypted output file: %s\n", strerror(errno));
		retVal = -1;
		goto end;
	}

	retVal = ske_decrypt(decrypted, cipher + offset_in, inputFileSize - offset_in, K);
	munmap(cipher, inputFileSize);
	munmap(decrypted, outputFileSize);

end:
	close(outFile);
outFailed:
	close(inFile);
inFailed:
	return retVal;
}
